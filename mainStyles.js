import {StyleSheet} from 'react-native';
import moment from 'moment-jalaali';

export const PersianNumbers = ['۰', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];

// export const Colors = {
//   Gold : '#fec821',
//   WeakGold: '#F8DE7E',

//   Back : '#272121',
//   WeakBack : '#363333',
//   StrongFocus : 'white',
//   WeakFocus : '#f6e9e9',
//   VeryWeakFocus: '#f8f1f1',

//   WeakAlarm: '#CD5C5C',
//   Alarm: 'red',
//   Submit: '#A9BA9D',
//   Info: '#555555'
// }

export const Colors = {
  Gold : '#fec821',
  WeakGold: '#F8DE7E',
  Back : '#F5F5F5',
  WeakBack : '#FFFFFF',
  StrongFocus : '#050001',
  WeakFocus : '#59584c',
  VeryWeakFocus: '#D3D3D3',
  WeakAlarm: '#CD5C5C',
  Alarm: 'red',
  Submit: '#A9BA9D',
  Info: '#ffffe0'
}

export const ShadowStyle = {
  Color: Colors.StrongFocus,
  Offset: { width: 10, height: 10 },
  Opacity: 1,
  Radius: 10,
  elevation: 2,
  
}

export const MenuProviderStyles = {
  menuProviderWrapper: {
    borderRadius: 10,
  },
  backdrop: {
    backgroundColor: 'black',
    opacity: 0.6,
  },
};

export function TranslateToPersianNumbers(text) {
  if(text != null || text != undefined)
  {
    var chars = text.split('');
    for (var i = 0; i < chars.length; i++) {
        if (/\d/.test(chars[i])) {
            chars[i] = PersianNumbers[chars[i]];
        }
    }
    return chars.join('');
  }
}

export function ToJalali(date) {
  if(date != null)
    return moment(date).format('jYYYY/jM/jD')
}

export function TranslatePropertyName(name){

  var result = ''
  switch(name) {
    case "size":
      result = 'سایز'
      break;
    case "weight":
      result = 'وزن'
      break;
    case "color":
      result = 'رنگ بندی'
      break;
    case "fee":
      result = 'اجرت'
      break;
    default:
  }

  return result;
}

export const EditPageStyles = StyleSheet.create({
  InfoSectionView: {
    padding: 5,
    backgroundColor: Colors.WeakBack,

    shadowColor: ShadowStyle.Color,
    shadowOffset: ShadowStyle.Offset,
    shadowOpacity: ShadowStyle.Opacity,
    shadowRadius: ShadowStyle.Radius,
    elevation: ShadowStyle.elevation,
  },
  InfoSectionSeperator: {
    alignItems: 'center',
    marginLeft: 40,
    marginRight: 40,
  },

  InfoSectionPartView: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionTitleView: {
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  
  VariantSectionTagView: {
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionRouteView: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionPropertyView: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.Info,
    margin:  0,
    padding: 5,
    paddingRight: 20,
    paddingLeft: 20,
    minHeight: 60,
  },

  AddVariantView: {
    margin: 5,
    padding: 5,
    alignItems : 'center'
  },

  AddRouteView:{
    margin: 10,
    padding: 10,
  },

  InfoSectionLableText: {
    fontSize: 15,
    marginLeft: 10,
  }
})

export const UserPageStyles = StyleSheet.create({
  InfoSectionView: {
    padding: 5,
    backgroundColor: Colors.WeakBack,

    shadowColor: ShadowStyle.Color,
    shadowOffset: ShadowStyle.Offset,
    shadowOpacity: ShadowStyle.Opacity,
    shadowRadius: ShadowStyle.Radius,
    elevation: ShadowStyle.elevation,
  },
  InfoSectionSeperator: {
    alignItems: 'center',
    marginLeft: 40,
    marginRight: 40,
  },

  InfoSectionPartView: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionTitleView: {
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  
  VariantSectionTagView: {
    flexDirection: 'row-reverse',
    justifyContent: 'center',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionRouteView: {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin:  0,
    padding: 0,
    minHeight: 60,
  },

  VariantSectionPropertyView: {
    flexDirection: 'row-reverse',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: Colors.Info,
    margin:  0,
    padding: 5,
    paddingRight: 20,
    paddingLeft: 20,
    minHeight: 60,
  },

  AddVariantView: {
    margin: 5,
    padding: 5,
    alignItems : 'center'
  },

  AddRouteView:{
    margin: 10,
    padding: 10,
  },

  InfoSectionLableText: {
    fontSize: 15,
    marginLeft: 10,
  }
})


export const CreateRecordStyles = StyleSheet.create({

  MainView: {
    padding: 10,
  },

  ButtonView: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  }, 

  CreateButton: {
    height: 40,
    padding: 10
  }

})

export const CategorySectionStyles = StyleSheet.create({

  MainView : {
    direction: 'rtl',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  }
})

export const PeoplePageStyles = StyleSheet.create({
  
  SelectedItemView: {
    alignSelf: 'center',
    borderColor: Colors.WeakFocus,
    borderRadius: 8,
    alignContent: 'center',
    backgroundColor: Colors.WeakBack,
    flexDirection: 'row-reverse', 
    justifyContent: 'space-between',
    height: 160,
    width : '97%',

    shadowColor: ShadowStyle.Color,
    shadowOffset: ShadowStyle.Offset,
    shadowOpacity: ShadowStyle.Opacity,
    shadowRadius: ShadowStyle.Radius,
    elevation: ShadowStyle.elevation,
  },

})

export const PurchasePageStyles = StyleSheet.create({
  PageScrolView: {
    backgroundColor: Colors.Back
  },

  DescriptionTextInput: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 10,
    textAlign:'right',
    padding: 5,
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
    borderWidth: 0.3,
    fontSize: 18,
    backgroundColor: Colors.WeakBack,
  },

  PurcaseSectionTitle: {
    padding: 10,
    fontSize: 16,
  },

  PurchaseButton: {
    height: 50,
    margin: 10,
  },

  PurchaseSectionView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    //borderWidth: 1,
    padding: 5,
  },

  HistorySectionView: {
    //borderWidth: 1,
  },

  HistorySectionTitle: {
    padding: 10,
    fontSize: 16,
  },

  EntityView : {
    flexDirection: 'row-reverse',
    justifyContent: 'space-between',
    backgroundColor: Colors.WeakBack,
    margin: 5,
    borderRadius: 7,
    overflow: 'hidden',

    shadowColor: ShadowStyle.Color,
    shadowOffset: ShadowStyle.Offset,
    shadowOpacity: ShadowStyle.Opacity,
    shadowRadius: ShadowStyle.Radius,
    elevation: ShadowStyle.elevation,
  },

  EntityImageView: {
    flex: 3,
    overflow: 'hidden'
  },

  EntityDetailsView:{
    flex: 4,
  },

  EntityDetailsText: {
    padding: 10,
    borderWidth: 1,
  },

  EntityRemoveButtonView: {
    flex : 1,
    justifyContent: 'center'
  },

  EntityImage: {
    width: '100%',
    height: '100%'
  },

  EntityRemoveTouchable: {
    width: '100%',
    height: '100%',
    alignSelf: 'center',
    justifyContent: 'center'
  },

  EntityRemoveImage: {
    width: '40%',
    height: '40%',
    tintColor: Colors.WeakFocus,
    alignSelf: 'center',
  }
})


export const DrawerStyles = StyleSheet.create({

  View: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 7,
  },

  TopView: {

  },

  BotView: {

  },

  ProfileSectionView: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: '100%',
    height: 150,
    backgroundColor : Colors.WeakBack,
    marginBottom: 10,
    borderRadius: 7,
  },

  ProfileSectionTopView: {
    flex: 2,
    //borderWidth: 1,
    flexDirection: 'row',
  },

  ProfileSectionBotView: {
    flex: 1,
    //borderWidth: .3,
    padding: 10,
  },

  ProfileSectionAvatarView: {
    margin: 5,
    borderWidth: .8,
    flex: .8,
    backgroundColor: Colors.WeakGold,
  },

  ProfileSectionAvatar: {
    alignSelf: 'center',
    width: '100%',
    height: '100%'
  },

  ProfileSectionPersonDetailsView: {
    flex: 2,
    padding: 10,
    direction: 'rtl',
    //borderWidth: 1,
  },  

  ProfielSectionPersonDetailsName:{
    flexDirection: 'row-reverse',
    justifyContent: 'space-between'
  },

  PurchasedSectionView: {
    width: '100%',
    //height: '100%',
    backgroundColor : Colors.WeakBack,
    marginBottom: 10,
    borderRadius: 7,
  },
})

export const UserPageStyle = StyleSheet.create({

  MainView: {
    flexDirection: 'column',
    padding: 3,
    backgroundColor: Colors.Back
  },

  TopView: {
    flexDirection: 'column',
    padding: 3,

  },
})

export default StyleSheet.create({

  Shadow: {
    shadowColor: ShadowStyle.Color,
    shadowOffset: ShadowStyle.Offset,
    shadowOpacity: ShadowStyle.Opacity,
    shadowRadius: ShadowStyle.Radius,
    elevation: ShadowStyle.elevation,
  },
  

    container: {
      padding: 10,
      alignItems:'center',
      justifyContent:'center',
      backgroundColor: '#43a1c9',
    },

    buttonText: {
      fontSize: 20,
      textAlign: 'center'
    },

    activityButtons: {
      tintColor: Colors.VeryWeakFocus,
      width: 24,
      height: 24,
      padding: 0,
      margin: 3,
      marginRight: 10,
      marginLeft: 10,
      alignSelf: 'center',
      resizeMode: 'contain',
    },

    persian:{
      textAlign:'right'
    },

    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
    },
    
    instructions: {
      textAlign: 'center',
      color: Colors.Gold,
      marginBottom: 5,
    },

    // SEARCH PAGE //=========================================================================

    PostsText: {
      textAlign:'right',
      padding: 10,
      margin: 10
    },

    SearchPageGridView: {
      flex: 1,
      backgroundColor: Colors.Back,
    },

    SearchPageItemContainer: {
      borderRadius: 4,
      backgroundColor: Colors.WeakBack,
      height: 120,

      shadowColor: ShadowStyle.Color,
      shadowOffset: ShadowStyle.Offset,
      shadowOpacity: ShadowStyle.Opacity,
      shadowRadius: ShadowStyle.Radius,
      elevation: ShadowStyle.elevation,
    },

    SearchPageItemText: {
      alignSelf: 'center',
      color: Colors.StrongFocus,
      fontWeight: 'bold',
    },

    SearchPageItemImage: {
      borderRadius: 7,
      alignSelf: 'center',
      height: 100,
      width: '100%',
  },

    SearchPageSectionTitle: {
        direction: 'rtl',
        fontWeight: 'bold',
        color: Colors.WeakFocus,
        fontSize: 17,
        padding: 6,
        paddingRight: 17
    },
    SearchPageSectionView: {
        //backgroundColor: ColorFocusForMain,
        height: 30,
        //borderBottomWidth: 1,
        //borderColor: ColorFocusForMain,
        backgroundColor: Colors.WeakBack,
        borderColor: Colors.WeakFocus,
        borderBottomWidth: 0.4,
        borderRadius: 0,
        marginBottom: 5,
    },

    SearchPageSectionHeaderTicket: {
      position: 'absolute',
      width: 30,
      height: 30,
      marginTop: 0,
      marginLeft: 10,
      tintColor: Colors.WeakFocus
    },

    SearchPageSearchSection : {
      flex: 1,
      flexDirection: 'row',
    },

    SearchPageSearchIcon : {
      position: 'absolute',
      width: 40,
      height: 40,
      marginTop: 15,
      marginLeft: 10,
    },

    SearchPageTextInput : {
        flex: 1,
        height: 46,
        paddingLeft: 10,
        paddingRight: 10,
        textAlign:'right',
        padding: 5,
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        borderBottomWidth: 0.7,
        fontSize: 18,
        backgroundColor: Colors.WeakBack,
    },

    // SEARCH PAGE ================================================

    // RECORD ENTITY =======================================

    RecentPageRecordEntityView : {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'stretch',
      backgroundColor: Colors.WeakBack,

      shadowColor: ShadowStyle.Color,
      shadowOffset: ShadowStyle.Offset,
      shadowOpacity: ShadowStyle.Opacity,
      shadowRadius: ShadowStyle.Radius,
      elevation: ShadowStyle.elevation,
    },

    RecordEntityText : {
      textAlign:'right',
      padding: 10,
      margin: 10
    },

    RecentPageRecordEntityImage: {
      height: 300,
      width: '100%',
    },

    RecentPageRecordSwipperView: {
      minHeight: '0%',
      height: 300,
    },

    RecentPageRecordSlideView: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },

    RecentPageRecordActivityBarView: {
      borderTopWidth: 0.2,
      borderColor: Colors.StrongFocus,
      justifyContent: 'space-between',
      paddingTop: 10,
      padding: 3,
      height: 40,
      flexDirection: 'row',
    },

    RecordEntityDetailsView: {
      flexDirection : 'column',
      backgroundColor: Colors.Info,
    },

    RecordEntityDetailsPartView: {
      flexDirection: 'row-reverse',
      justifyContent: 'space-between',
    },

    RecordEntityDetailsPartRightChildren: {
      flexDirection: 'row-reverse',
      marginLeft: 7
    },
    
    RecordEntityDetailsPartLeftChildren: {
      flexDirection: 'row',
      marginLeft: 7
    },


    RecordEntityDetailsPartChildView: {
      justifyContent: 'flex-start',
      alignItems: 'center',
      flexDirection: 'row-reverse',
    },

    RecordEntityDetailsLabel:{
      fontSize: 13,
      margin: 3,
      fontWeight: 'normal',
      color: Colors.WeakFocus
    },

    RecordEntityDetailsValue: {
      fontSize: 15,
      fontWeight: 'bold',
      color: Colors.StrongFocus
    },

    RecordEntityCommentSectionView:{
      flexDirection : 'column',
      margin: 10,
      backgroundColor: Colors.WeakBack,
    },

    RecordEntityCommentView: {
      flexDirection: 'row-reverse',
      alignItems: 'flex-start',
    },

    RecordEntityCommentUser:{
      marginLeft: 5,
      marginRight: 5,
      fontStyle: 'italic',
      fontWeight: 'bold',
      fontSize: 12,
      flexWrap: 'wrap',
      alignSelf : 'flex-start'
    },

    RecordEntityStatisticComment:{
      margin: 6,
      fontWeight: 'bold',
      fontSize: 12,
      flexWrap: 'wrap',
      alignSelf : 'flex-start',
      color: Colors.VeryWeakFocus
    },

    RecordEntityCommentText:{
      fontSize: 13,
      fontWeight: 'normal',
      flex: 1, 
      flexWrap: 'wrap'
    },

    // ============================== Hierarchy Category =================================

    HierarchyCategoryIndexingView: {
      justifyContent: 'flex-start',
      flexDirection: 'row-reverse',
      flex: 1,
      padding: 10,
    },

    HierarchyCategoryIndexingNextView: {
      justifyContent: 'flex-start',
      flexDirection: 'row-reverse',
      flex: 1,
      padding: 3,
    },

    HierarchyCategoryScrollView: {
      flexDirection: 'row-reverse', // rtf ltr
    },

    HierarchyCategoryIndexingWhole: {
      flexDirection: 'column',
      padding: 10,
    },

    HierarchyCategoryButtons: {
      flexDirection: 'row',
    },

    HierarchyCategoryButtonText: {
      fontSize: 18,
      padding: 5,
      color: Colors.WeakFocus
    },

    HierarchyCategoryItemBackground: {
      backgroundColor: Colors.WeakGold,
      borderRadius: 7,

      shadowColor: ShadowStyle.Color,
      shadowOffset: ShadowStyle.Offset,
      shadowOpacity: ShadowStyle.Opacity,
      shadowRadius: ShadowStyle.Radius,
      elevation: ShadowStyle.elevation,

      margin: 1,
    },

    HierarchyCategoryTicket: {
      tintColor: Colors.WeakFocus,
      position: 'absolute',
      width: 13,
      height: 13,
      paddingRight: 2,
    },

    HierarchyCategoryNextButtons: {
      padding: 0,
      borderRadius: 3,
      margin: 3,
    },

    HierarchyRow: {
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1, 
      borderColor: Colors.Gold, 
      padding: 2, 
      margin: 5, 
      flex:1, 
      flexDirection: 'column'
    },

    TabbarMenuIcon: {
      tintColor: Colors.VeryWeakFocus,
      marginLeft: 0,
      width:30, 
      height:30, 
    },

    tabBarIcon: {
      width: 30, 
      height:30, 
    },

    ButtonText: {
      alignSelf: 'center',
      fontWeight: 'bold',
      fontSize: 17,
      color: Colors.WeakFocus
    },

    ButtonBack: {
      backgroundColor: Colors.WeakGold,
      borderRadius: 8,
      justifyContent: "space-around",
      height: 40,

      shadowColor: ShadowStyle.Color,
      shadowOffset: ShadowStyle.Offset,
      shadowOpacity: ShadowStyle.Opacity,
      shadowRadius: ShadowStyle.Radius,
      elevation: ShadowStyle.elevation,

      margin: 0,
    },

    input: {
      paddingLeft: 10,
      paddingRight: 10,
      textAlign:'right',
      justifyContent: 'flex-start',
      padding: 10,
      marginTop: 10,
      marginLeft: 10,
      marginRight: 10,
      marginBottom: 10,
      borderBottomWidth: 0.7,
      fontSize: 18,
      backgroundColor: Colors.WeakBack,
    },
});
  