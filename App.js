import React, {Component} from 'react';
import {Image, Dimensions, StatusBar, AsyncStorage } from 'react-native';
import {createDrawerNavigator, createBottomTabNavigator, createAppContainer, createStackNavigator} from 'react-navigation';
import {MenuProvider} from 'react-native-popup-menu';

import MainStyles, { Colors , MenuProviderStyles, ShadowStyle} from './mainStyles';
import {NetworkStats} from './network';
import PeoplePage from './components/pages/peoplePage';
import SearchPage from './components/pages/searchPage';
import RecentPage from './components/pages/recentPage';
import LoginPage from './components/pages/loginPage';
import EditPage from './components/pages/editPage';
import PurchasePage from './components/pages/purchasePage';
import Sidebar from './components/sidebar';
import UserPage from './components/pages/userPage';
import AdminPage from './components/pages/adminPage';

export var apiLink = 'http://198.143.181.218:3000/api/';
export var navigationReference = '';
export const Context = React.createContext("default");

const { width, height } = Dimensions.get('screen');

class HomePage extends RecentPage {
  constructor(props) {
    super(props);
    navigationReference = this.props.navigation;
  }

  render()
  {
    return <RecentPage/>
  }
}

export const BottomTab = createBottomTabNavigator(
{
  recent:  HomePage,
  search: SearchPage,
  purchase: PurchasePage,
  people: PeoplePage,
  منو: {
    screen: (arg) => arg.navigation.toggleDrawer(),
		navigationOptions: {
      tabBarOptions: {
        showLabel: true
      },
			tabBarIcon: (
        <Image style = {MainStyles.TabbarMenuIcon} source={require('./resources/Icons/tab_bar_menu.png')}/>
      ),
			tabBarOnPress: (arg) => {
        arg.navigation.toggleDrawer()
      },
      tabBarOnLongPress: (arg) => {
        arg.navigation.toggleDrawer()
      }
		}
  }
},
{  
  tabBarOptions: {
    initialRouteName : 'recent',
    activeTintColor: Colors.StrongFocus,
    inactiveTintColor: Colors.VeryWeakFocus,
    activeBackgroundColor: Colors.WeakGold,
    showLabel: false,
    showIcon: true,
    
    style:{
      shadowColor: ShadowStyle.Color,
      shadowOffset: ShadowStyle.Offset,
      shadowOpacity: ShadowStyle.Opacity,
      shadowRadius: ShadowStyle.Radius,
      elevation: ShadowStyle.elevation,
    },
    labelStyle: {
      fontSize: 14
    },
    iconStyle: {
      height: 23,
      width: 23
    }
  },
}
);

export const BottomTabUser = createBottomTabNavigator(
  {
    recent:  HomePage,
    search: SearchPage,
    purchase: PurchasePage,
    منو: {
      screen: (arg) => arg.navigation.toggleDrawer(),
      navigationOptions: {
        tabBarOptions: {
          showLabel: true
        },
        tabBarIcon: (
          <Image style = {MainStyles.TabbarMenuIcon} source={require('./resources/Icons/tab_bar_menu.png')}/>
        ),
        tabBarOnPress: (arg) => {
          arg.navigation.toggleDrawer()
        },
        tabBarOnLongPress: (arg) => {
          arg.navigation.toggleDrawer()
        }
      }
    }
  },
  {  
    tabBarOptions: {
      initialRouteName : 'recent',
      activeTintColor: Colors.StrongFocus,
      inactiveTintColor: Colors.VeryWeakFocus,
      activeBackgroundColor: Colors.WeakGold,
      showLabel: false,
      showIcon: true,
      
      style:{
        shadowColor: ShadowStyle.Color,
        shadowOffset: ShadowStyle.Offset,
        shadowOpacity: ShadowStyle.Opacity,
        shadowRadius: ShadowStyle.Radius,
        elevation: ShadowStyle.elevation,
      },
      labelStyle: {
        fontSize: 14
      },
      iconStyle: {
        height: 23,
        width: 23
      }
    },
  }
  );

const MainDrawer = createDrawerNavigator(
  {
    Main: {
      screen: BottomTab
    }
  },
  {
    drawerType: 'slide',
    drawerPosition: 'right',
    contentComponent: Sidebar,
    drawerWidth: Math.min(height, width) * 0.8,
    drawerBackgroundColor: Colors.Back
  },
);

const MainDrawerUser = createDrawerNavigator(
  {
    Main: {
      screen: BottomTabUser
    }
  },
  {
    drawerType: 'slide',
    drawerPosition: 'right',
    contentComponent: Sidebar,
    drawerWidth: Math.min(height, width) * 0.8,
    drawerBackgroundColor: Colors.Back
  },
);

const MainStack = createStackNavigator(
  {
    main: {
      screen: MainDrawer,
      navigationOptions: {
        header: null,
      }
    },
    edit: {
      screen: EditPage,
      navigationOptions: {
        header: null,
      }
    },

    login: {
      screen: LoginPage,
      navigationOptions: {
        header: null,
      }
    },

    user: {
      screen: UserPage,
      navigationOptions: {
        header: null
      }
    },
    admin: {
      screen: AdminPage,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    mode: 'card'
  }
)

const MainStackUser = createStackNavigator(
  {
    main: {
      screen: MainDrawerUser,
      navigationOptions: {
        header: null,
      }
    },
    edit: {
      screen: EditPage,
      navigationOptions: {
        header: null,
      }
    },

    login: {
      screen: LoginPage,
      navigationOptions: {
        header: null,
      }
    },

    user: {
      screen: UserPage,
      navigationOptions: {
        header: null
      }
    },
    admin: {
      screen: AdminPage,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    mode: 'card'
  }
)

const RootNavigator = createAppContainer(MainStack);

const RootNavigatorUser = createAppContainer(MainStackUser);


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      profileData : {
        "address": "",
        "companynumber": "",
        "firstname": "",
        "lastname": "",
        "middlename": "",
        "mobilenumber": "",
        "owner": 0,
        "phonenumber": "",
        "profilepicturethumbnail": "http://www.scified.com/images/defaultavatar.png",
        "profilepictureurl": "http://www.scified.com/images/defaultavatar.png"
      },
      loadProfile: this.loadProfile,
      clearProfile: this.clearProfile,
      signedIn: false,
      purchases: [],
      updatePurchases : this.updatePurchases,
      isAdmin : false,
      initialized: false,
      
    }

  }

  componentWillMount(){
    this.retrieveData().then(() => {
      this.setState({initialized: true})
    })
  }

  refreshStat = () =>
  {
    this.setState(this.state)
  }

  clearProfile = () => 
  {
    this.setState({signedIn : false, profileData: {
      "address": "...",
      "companynumber": "...",
      "firstname": "...",
      "lastname": "...",
      "middlename": "...",
      "mobilenumber": "...",
      "owner": 0,
      "phonenumber": "...",
      "profilepicturethumbnail": "http://www.scified.com/images/defaultavatar.png",
      "profilepictureurl": "http://www.scified.com/images/defaultavatar.png"
    }})
  }

  loadProfile = (data) =>
  {
    this.setState({profileData: data, signedIn: true})
  }

  updatePurchases = (data) =>
  {
    this.setState({purchases: data})
  }

  retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('token');
      if (value !== null) {
        token = value;

        var userId = await AsyncStorage.getItem('userId');
        if(userId == 3){
          this.setState({isAdmin: true})
        }
      }
      else
      {
        navigationReference.navigate('login')
      }
     } catch (error) {
       console.log(error);
     }
  }

  render() {
    StatusBar.setTranslucent(false)
    return (
      <Context.Provider value ={this.state}>
        <MenuProvider backHandler = {true} customStyles={ MenuProviderStyles}>
            {this.state.isAdmin?<RootNavigator isAdmin = {true}/>:<RootNavigatorUser isAdmin = {false}/>}
            <NetworkStats/>
        </MenuProvider>
      </Context.Provider>
    );
  }
}

