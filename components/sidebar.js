import React from 'react';
import {View, BackHandler, AsyncStorage} from 'react-native';

// Styles
import MainStyles, {DrawerStyles,Colors} from '../mainStyles';
import ProfileSection from './profileSection';
import Button from './base/button';
import { Context ,navigationReference} from '../App';


export default class Sidebar extends React.Component {

  exitFunction = () =>
  {
    BackHandler.exitApp();
  };

  changeColor = () =>
  {
    Colors.Back = 'black';
    Colors.WeakBack = 'grey';
    Colors.StrongFocus = 'white';
    Colors.WeakFocus = 'brown';
    this.setState(this.state);
  };

  render() {
      return (
        <Context.Consumer>
          {data => <View style={DrawerStyles.View}>
            <View style={DrawerStyles.TopView}>
              <ProfileSection isAdmin ={(data.profileData.owner == 3)?(true):(false)} loadProfile = {data.loadProfile}/>
              {data.profileData.owner == 3 ? (
              <View>
                <Button style ={{height: 60}} title = "گزارش گیری" onPress ={() =>{
                  navigationReference.navigate('admin')
                }}>
                </Button>
                </View>
            ):(
              null
            )}
            </View>
            <View style={DrawerStyles.BotView}>
              {data.signedIn? <Button title ="خروج" onPress = {() => {
                AsyncStorage.removeItem("token").then(()=>{
                  AsyncStorage.removeItem("userId").then(()=>{
                    data.clearProfile()
                  })
                })}}></Button> : 
                <Button title ="ورود!" onPress = {() => {
                  navigationReference.navigate('login')
                  }}></Button>
                }
            </View>
          </View>}
        </Context.Consumer>
      );
    }
  }