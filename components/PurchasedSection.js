import React from 'react';
import {View, TouchableOpacity, Image, Text, Button, BackHandler} from 'react-native';
import {CachedImage} from 'react-native-cached-image';

// Styles
import MainStyles, {Colors, DrawerStyles} from '../mainStyles';
  
export default class PurchasedSection extends React.Component {
  render(){
    return(
    <View style ={DrawerStyles.PurchasedSectionView}>
      <TouchableOpacity 
          onPress={this.exitFunction}>
          <Text>Purchased</Text>
      </TouchableOpacity>
    </View>
    )}
  }