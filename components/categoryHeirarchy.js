import React from 'react';
import {View, TouchableOpacity, Image, Text, ScrollView, ImageBackground, Alert} from 'react-native';
import {CachedImage} from '../network';
import Swiper from 'react-native-swiper';
import { CategoryRepository} from '../repository';

// Styles
import MainStyles, {Colors, TranslateToPersianNumbers} from '../mainStyles';
import Button from './base/button';

export default class CategoryHierarchy extends React.Component
{
    componentDidMount() {
        this.retrieveCategories();
        this.props.setCategoryRefresher(() => this.retrieveCategories(true))
    };

    gaugeLayers(routes, index, result)
    {
        dic = {}
        remainingRoots = []
        for(i = 0; i < routes.length; i++)
        {
            var route = routes[i]

            if(index + 1 == route.length) // END POINT
                result.push((route.slice(0, index + 1).join(',')  + ':'))
            else
            {
                dichead = route.slice(0, index + 1).join(',')
                dicend = route[index + 1]
                if(dichead in dic) // ROOT CATEGORIES
                {
                    if(!dic[dichead].includes(dicend))
                    {
                        dic[dichead].push(dicend)
                    }
                }
                else
                    dic[dichead] = [dicend]
                
                remainingRoots.push(route)
            }
        }

        for(var key in dic)
        {
            result.push((key + ':' + dic[key].join(',')))
        }

        if(remainingRoots.length > 0)
            this.gaugeLayers(remainingRoots, index + 1, result)
    }

    serverTolocal(serverData)
    {
        if(!(serverData == null || serverData === undefined))
        {
            if(serverData.length > 0)
            {
                result = []
                routes = serverData.map(x=> {
                    g = x.route.split('-')
                    if(g[0] != ' ')
                    {
                        return g;
                    }
                });

                routes = routes.filter(function (el) {
                    return el != null;
                });


                this.gaugeLayers(routes, 0,  result)

                // REMOVE tags with categories confglix model: and model:A,B for instance, showing model twice
                newResult = []
                for(i = 0; i < result.length; i++)
                {
                    c = result[i]
                    f = newResult.filter(x => {
                        return x.split(':')[0] == c.split(':')[0]
                    })

                    if(f.length > 0)
                    {
                        if(c.split(':')[1] > f[0].split(':')[1])
                        {
                            var index = newResult.indexOf(f[0])
                            newResult.splice(index, 1)
                            newResult.push(c)
                        }
                    }
                    else
                        newResult.push(c)
                }


                this.props.updateCategoryData(newResult)
            }
        }
    }

    localToServer(localData)
    {

    }

    retrieveCategories = (forced) => {
        CategoryRepository.getCategories(forced).then((categories)=>
        {
            this.serverTolocal(categories)
        })

        // SAMPLE of how this ui works
        // this.setState({data : [
        //     "همه,النکو,گنجه:",
        //     "همه,النکو,عنجه:",
        //     "همه,ملنگو:شش,مش",
        //     "همه:النکو,ملنگو,پلنگو",
        //     "همه,النکو:پنجه,گنجه,عنجه",
        //     "همه,النکو,پنجه:الفا,بتا",
        //     "همه,النکو,پنجه,الفا:",
        //     "همه,النکو,پنجه,بتا:",
        //     "همه,ملنگو,شش:",
        //     "همه,ملنگو,مش:",
        //     "همه,پلنگو:",
        //     "عیار:17,24",
        //     "عیار,17:",
        //     "عیار,24:",
        //     "موجود:",
        // ]})
    };

    nextItemHandler = (entry) => {
        counter = 0;
        var mathed = false;
        var added = false;
        this.props.selected.forEach(selectedPath =>  {
            const temp = selectedPath + "," + entry;
            var parts = selectedPath.split(',');
            this.props.data.forEach(element => {
                if(temp == element.split(':')[0])
                {
                    this.props.selected[counter] = temp;
                    this.props.updateCategorySelectedData(this.props.selected)
                    added =  true;
                    return;
                }

                if(parts.includes(entry))
                {
                    mathed = true;
                }
            })

            counter++;

            
            if(added)
                return;
        })

        if(mathed == false && added == false)
        {
            this.props.selected[counter++] = entry;
            this.props.updateCategorySelectedData(this.props.selected)
        }
    }

    currentItemHandler = (entry) => {
        for (i = 0; i < this.props.selected.length; i++) { 
            var selectedPath = this.props.selected[i];

            const parts = selectedPath.split(',');
            var lastPart = parts[parts.length-1];
            if(lastPart == entry)
            {
                if(parts.length == 1)
                {
                    this.props.selected.splice(i, 1);
                    this.props.updateCategorySelectedData(this.props.selected)
                    break;
                }
                else if(parts.length > 1)
                {
                    parts.splice(parts.length-1,1);
                    this.props.selected[i] = parts.join(',');
                    this.props.updateCategorySelectedData(this.props.selected)
                    break;
                }
            }
        }
    }

    deleteCategoryHandler = (name) =>{
        if(this.props.enableEditing == false)
            return

        var fullname;
        if(this.props.selected.length > 1)
        {
            Alert.alert('برای پاک کردن دسته فقط یک مسیر دسته را انتخاب کنید')
        }
        else{

            if(this.props.selected.length == 0)
                fullname = name;
            else   
                fullname = this.props.selected[0].split(',').join('-') + '-' + name;

            Alert.alert(
                'اخطار',
                'آیا مطمین به پاک کردن دسته ی ' + fullname + ' هستید؟ ',
                [
                {text: 'خیر', onPress: () => {
                    console.log('Cancel Pressed')

                }, style: 'cancel'},

                {text: 'بله', onPress: () => {
                    if(this.checkEndPoint(fullname))
                    {
                        CategoryRepository.DeleteCategory(fullname.split('-')[0], fullname).then(() =>
                        {
                            //refresh
                            this.props.categoryRefresher()
                        })
                    }
                    else{
                        Alert.alert('این دسته زیر مجموعه دارد. اول زیر مجموعه هارا پاک کنید')
                    }

                }},],{ cancelable: false }
            )
        }
    }

    checkEndPoint(name) {
        var f = this.props.data.filter(x => {
            return (x.split(':')[0] == name.split('-').join(',') && x.split(':')[1].length > 0)
        })

        if(f.length > 0)
            return false
        else 
            return true
    }

    renderHiararchy = () => {
        if(this.props.data.length == 0)
            return

        rows = [];
        rowsCounter = 0;
        heads = [];
        headCounter = 0;
        this.props.data.forEach((element, index) => {
            var whole = element.split(':');
            var pre = whole[0];
            var preSplited = whole[0].split(',');
            var headOf = this.props.selected.find((element) => {return element.split(',')[0] == preSplited[0]})

            if(preSplited.length == 1 && headOf == null) // Heads
            {
                heads[headCounter++] = <CategoryHierarchyNextItem 
                key ={'haed' + index}
                title = {preSplited[0]} 
                pressHandler = {this.nextItemHandler}
                longPressHanlder = {this.deleteCategoryHandler}/>
            }
        })

        rows[rowsCounter++] = <View style ={{flex:1, flexDirection: 'row-reverse'}}>{heads}</View>

        this.props.selected.forEach((element, index) => {
            var column = []
            var columnCounter = 0;
            var selected = []
            var selectedCounter = 0;
            var relatedData = this.props.data.find((d) => {return d.split(':')[0] == element})
            var selectedParts = element.split(',')


            selectedParts.forEach((part, partIndex) => {
                if(selectedParts.indexOf(part) == selectedParts.length-1)
                {
                    selected[selectedCounter++] = <CategoryHierarchyItem isLast = {true} key ={'element' + index + '/' + partIndex} title = {part} pressHandler = {this.currentItemHandler}/>;
                }
                else
                    selected[selectedCounter++] = <CategoryHierarchyItem key ={'element' + index + '/' + partIndex} title = {part} pressHandler = {this.currentItemHandler}/>;
            });

            column[columnCounter++] = <View style ={{flex:1, flexDirection: 'row-reverse'}}>{selected}</View>;
            column[columnCounter++] = <View style ={{width: 50}}></View>;

            relatedData.split(':')[1].split(',').forEach((part, partIndex) => {
                if(part != "")
                {
                    column[columnCounter++] = <CategoryHierarchyNextItem 
                        key ={'part' + partIndex}
                        title = {part} 
                        pressHandler = {this.nextItemHandler}
                        longPressHanlder = {this.deleteCategoryHandler}/>
                }
            })

            rows[rowsCounter++] = <View style ={MainStyles.HierarchyRow}>{column}</View>
        })

    

        var output = <View style ={{flex:1, flexDirection: 'column'}}>{rows}</View>

        return output;
    }

    render()
    {
        return(
            <View>
                {this.renderHiararchy()}  
            </View>
        );
    }
}

export class CategoryHierarchyItem extends React.Component
{
    renderTicket = (isLast) =>
    {
        if(isLast)
        return(
            <Image style = {MainStyles.HierarchyCategoryTicket} source ={require('../resources/Icons/hierarchy_item_ticket_close.png')}></Image>
        )
    }

    render()
    {
        return(
            <View style = {MainStyles.HierarchyCategoryButtons}>
                <TouchableOpacity onPress={() => this.props.pressHandler(this.props.title)}>
                    <Text style = {MainStyles.HierarchyCategoryButtonText} >{TranslateToPersianNumbers(this.props.title)}</Text>
                    {this.renderTicket(this.props.isLast)}
                </TouchableOpacity>
            </View>
        );
    }
}

export class CategoryHierarchyNextItem extends React.Component
{
    render()
    {
        return(
            <Button style ={MainStyles.HierarchyCategoryNextButtons} 
                onPress={() => this.props.pressHandler(this.props.title)} 
                onLongPress ={() => this.props.longPressHanlder(this.props.title)} 
                title = {TranslateToPersianNumbers(this.props.title)}
                textPadding = {10}
                 />  
        );
    }
}