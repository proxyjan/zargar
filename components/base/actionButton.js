import React from 'react';

import {Colors} from '../../mainStyles';
import Button from './button';

export default class ActionButton extends React.Component {

    render() {
        return (
            <Button 
                style ={{
                    borderRadius: 40,
                    position: 'absolute',
                    bottom: this.props.bottom, 
                    right: this.props.right,
                    top : this.props.top,
                    left: this.props.left,
                    height: this.props.height, 
                    width: this.props.width
                }} 
                onPress = {() => {this.props.onPress()}} 
                title = {this.props.title} 
                backColor = {Colors.WeakGold} 
                textColor = {Colors.WeakFocus} 
            />  
        );
      }
    }