import React from 'react';
import {TextInput} from 'react-native';

import MainStyles, {Colors} from '../../mainStyles'


export default class input extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            borderColor: Colors.WeakFocus
        }
    }

    onFocus =() => {
        this.setState({
            borderColor: Colors.Gold
        })
    }
    
    onBlur = () => {
        this.setState({
            borderColor: Colors.WeakFocus
        })
    }

    render()
    {
        return(
            <TextInput
            {...this.props}
            secureTextEntry = {this.props.secureTextEntry}
            value = {this.props.value}
            keyboardType = {this.props.keyboardType}
            multiline={this.props.isMultiLine}
            numberOfLines = {this.props.numberOfLines}
            onBlur={ () => this.onBlur() }
            onFocus={ () => this.onFocus() }
            onChangeText = {(text) => (this.props.onChangeHandler) ?(this.props.onChangeHandler(text)) : () => {}}
            placeholder = {this.props.placeHolder} 
            selectionColor = {Colors.WeakGold}
            autoFocus = {this.props.autoFocus}
            style = {[MainStyles.input, this.props.style, {borderColor: this.state.borderColor} ]}/>
        )
    };
}