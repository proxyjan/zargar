import React from 'react';
import {TouchableWithoutFeedback, Text, View, Modal} from 'react-native';
import Picker from 'react-native-picker';

import MainStyles,{Colors} from '../../mainStyles'


export default class Selector extends React.Component {
    constructor(props){
        super(props);
        this.state={
            modalVisible: false,
            selectedValue: [this.props.goodinfo.code[0],this.props.goodinfo.code[1],this.props.goodinfo.code[2],this.props.goodinfo.code[3],this.props.goodinfo.code[4]]
        };
    }

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    hide =() =>{
        this.setModalVisible(false)
        Picker.hide();     
        return true;
    }

    render() {
        this.renderPicker();
        return (
            <TouchableWithoutFeedback onPress={()=>{
            this.setModalVisible(true)  }}>
                <View> 
                    <View style ={{backgroundColor: Colors.WeakGold, borderWidth: .9, borderRadius: 6, padding: 5}}>
                        <Text style ={{ fontSize: 17, fontStyle : 'normal', fontWeight: 'bold'}}>
                            {this.props.goodinfo.code === undefined ? ('انتخاب') : (this.props.goodinfo.code.split('').join(' '))}
                        </Text>
                    </View>
                        <Modal
                        animationType="none"
                        transparent={true}
                        onShow ={Picker.show}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
                            this.setModalVisible(false) 
                            Picker.hide()
                        }}>
                        <TouchableWithoutFeedback 
                            onPressIn={() => {this.hide() }}>
                            <View style ={{width: '100%', height: '100%', backgroundColor: 'black', opacity: 0.6 }}></View>
                        </TouchableWithoutFeedback>
                    </Modal>
            </View>
           </TouchableWithoutFeedback>
        );
    }

    renderPicker=()=>{
        Picker.init({
            //[248, 222, 126, 1]
            pickerFontSize: 26,
            pickerConfirmBtnColor: [0, 0, 0, 1],
            pickerCancelBtnColor: [0, 0, 0, 1],
            pickerToolBarBg: [248, 222, 126, 1],
            pickerConfirmBtnText: 'انتخاب کنید',
            pickerTitleText: 'کد کالا',
            pickerCancelBtnText: 'انصراف',
            pickerData: this.props.data,
            selectedValue: this.state.selectedValue,
            onPickerConfirm: data => {
                this.setModalVisible(false) 
                this.props.goodinfo.code = data.join('')
                this.props.onValueChange(this.props.goodinfo)
            },
            onPickerCancel: data => {
                console.log(data);
                this.setModalVisible(false) 
            },
            onPickerSelect: data => {
                console.log(data);
            }
        });

        Picker.hide();        
    }
}