import React from 'react';
import {TouchableNativeFeedback, Text, View, Image} from 'react-native';

import MainStyles,{Colors} from '../../mainStyles'

export default class Button extends React.Component {

    render() {
        return (
            <View style={[MainStyles.ButtonBack, this.props.style, { overflow: 'hidden'}]}>
                <TouchableNativeFeedback 
                    useForeground = {true}
                    background={TouchableNativeFeedback.Ripple(Colors.Back, true)}
                    onLongPress = {this.props.onLongPress}
                    onPress = {this.props.onPress}>
                    
                    <View>
                        {this.props.IconPath?
                        (<Image source ={this.props.IconPath}/>):
                        (<Text style = {[MainStyles.ButtonText, {color: this.props.textColor}, {padding: this.props.textPadding}]}>{this.props.title}</Text>)}
                    </View>
                </TouchableNativeFeedback>
            </View>
        );
      }
    }