import React from 'react';
import {TouchableOpacity, Text, View} from 'react-native';

import MainStyles,{Colors} from '../../mainStyles'

export default class Check extends React.Component {

    render() {
        return (
            <TouchableOpacity onPress = {this.props.onPress}>
                <View style={{margin: 4, alignItems: 'center', padding: 5, borderRadius: 4, minWidth: 30, minHeight: 30, backgroundColor: this.props.value == true ? Colors.WeakGold : Colors.VeryWeakFocus}}>
                    <Text style ={{fontSize: this.props.value == true ? 15 : 14 , fontWeight: this.props.value == true ? 'bold' : 'normal'}}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableOpacity>
        );
      }
    }