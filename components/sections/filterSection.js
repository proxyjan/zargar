import React from 'react';
import {View, Text, Slider, Image} from 'react-native';
import MultiSlider from '@ptomasroos/react-native-multi-slider';

// Styles
import MainStyles, {Colors, CreateRecordStyles} from '../../mainStyles';
  
export default class FilterSection extends React.Component {
  render(){
    return(
      <View>
    <View style ={{flexDirection: 'row-reverse', justifyContent:'space-between', alignItems : 'center', paddingRight: '5%', paddingLeft: '5%'}}>
      <Text style ={{fontSize: 15, fontWeight: 'bold'}}>اجرت</Text>
      <MultiSlider 
        customMarkerLeft ={(e) => {
            return (<CustomMarker isMin = {true} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        customMarkerRight ={(e) => {
            return (<CustomMarker isMin = {false} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        isMarkersSeparated={true}
        values={this.props.valuesF} 
        selectedStyle ={{backgroundColor: Colors.Gold}}
        min = {this.props.minF}
        max = {this.props.maxF} 
        step ={10} 
        snapped = {true} 
        onValuesChangeFinish = {this.props.rangeHanlderF}
        enabledOne = {true} 
        enabledTwo = {true}/>
    </View>
    <View style ={{flexDirection: 'row-reverse', justifyContent:'space-between', alignItems : 'center', paddingRight: '5%', paddingLeft: '5%'}}>
      <Text style ={{fontSize: 15, fontWeight: 'bold'}}>وزن</Text>
      <MultiSlider 
        customMarkerLeft ={(e) => {
            return (<CustomMarker isMin = {true} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        customMarkerRight ={(e) => {
            return (<CustomMarker isMin = {false} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        isMarkersSeparated={true}
        values={this.props.valuesW} 
        selectedStyle ={{backgroundColor: Colors.Gold}}
        min = {this.props.minW}
        max = {this.props.maxW} 
        step ={10} 
        snapped = {true} 
        onValuesChangeFinish = {this.props.rangeHanlderW}
        enabledOne = {true} 
        enabledTwo = {true}/>
    </View>
    <View style ={{flexDirection: 'row-reverse', justifyContent:'space-between', alignItems : 'center', paddingRight: '5%', paddingLeft: '5%'}}>
      <Text style ={{fontSize: 15, fontWeight: 'bold'}}>سایز</Text>
      <MultiSlider 
        customMarkerLeft ={(e) => {
            return (<CustomMarker isMin = {true} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        customMarkerRight ={(e) => {
            return (<CustomMarker isMin = {false} pressed ={e.pressed} value ={e.currentValue}/>)
        }} 
        isMarkersSeparated={true}
        values={this.props.valuesS} 
        selectedStyle ={{backgroundColor: Colors.Gold}}
        min = {this.props.minS}
        max = {this.props.maxS} 
        step ={1} 
        snapped = {true} 
        onValuesChangeFinish = {this.props.rangeHanlderS}
        enabledOne = {true} 
        enabledTwo = {true}/>
    </View>
    </View>
    )}
  }

class CustomMarker extends React.Component {
    render() {
      return (
        <View style ={{alignItems: 'center', justifyContent: 'center',
            height: (this.props.pressed ? '80%' : '70%')}}>
            <Image 
                resizeMode = 'contain'
                style ={{
                    alignSelf: 'center',
                    height: '80%',
                    tintColor: (this.props.pressed ? Colors.Gold : Colors.WeakFocus)
            }}
            source={require('../../resources/Icons/ic_action_swap_horizontal_circle.png')}>
            </Image>
            <Text style ={{bottom: this.props.isMin ? '-80%' : '80%', position: 'absolute',
            bottom: this.props.pressed ? '120%' : ( this.props.isMin ? '-80%' : '80%')
         }}>{Math.floor(this.props.value)}</Text>
        </View>
      );
    }
  }