import React from 'react';
import {View, Text, Alert} from 'react-native';

import {GoodsRepository, CategoryRepository} from '../../repository';
import {selectedEntity} from '../recordEntity';

import {navigationReference} from '../../App';

// Styles
import MainStyles, {Colors, CreateRecordStyles} from '../../mainStyles';

// Components
import Input from '../base/input';
import Button from '../base/button';
  
export default class CreateRecord extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      textValue : ''
    }
  }

  render(){
    return(
    <View style = {CreateRecordStyles.MainView}>
      <View style = {CreateRecordStyles.ButtonView}>
        <Button onPress = {() => {
          if(this.state.textValue == '')
            return
          var newName;
          var newRoute;
          if(this.props.selected.length > 0)
          {
            newName = this.props.selected[0].split(',')[0];
            newRoute = this.props.selected[0].split(',').join('-') + '-' + this.state.textValue
          }
          else
          {
            newName = this.state.textValue
            newRoute = this.state.textValue
          }
          CategoryRepository.PostCategory({details: '{}', name: newName, route: newRoute}).then((x)=>
          {
            this.props.ctx.menuActions.closeMenu()
            this.props.categoryRefresher()
          })
        }}
        style = {CreateRecordStyles.CreateButton} title="اضافه کردن دسته"></Button>
        <Button onPress = {() => {
          if(this.state.textValue == '')
            return
          GoodsRepository.PostGood(this.state.textValue)
          .then((data)=>{
            this.props.ctx.menuActions.closeMenu()
            navigationReference.navigate("edit", {goodinfo: data})
          })
          .catch((r) => {
            Alert.alert(
              'خطا هنگام ساختن کالا',
              r)
          })
        }} 
          style = {CreateRecordStyles.CreateButton} title="اضافه کردن کالا"></Button>
      </View>
      <Input 
        onChangeHandler={(value) => this.setState({textValue: value})}
        value ={this.state.textValue} 
        autoFocus = {true} 
        placeHolder ="اسم"></Input>
    </View>
    )}
  }