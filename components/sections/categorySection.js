import React from 'react';
import {View, Text, Image} from 'react-native';

// Styles
import MainStyles, {Colors, CategorySectionStyles} from '../../mainStyles';

// Components
import Input from '../base/input';
import Button from '../base/button';
  
export default class CategorySection extends React.Component {
  renderCategoryPart(){
    result = []
    if(!(this.props.entity.hascategories === undefined))
    {
      categories = {}
      for (i = 0; i < this.props.entity.hascategories.length; i++) {
        category = this.props.entity.hascategories[i]
        name = category.categorynameref
        route = category.categoryrouteref

        if(route == name)
          continue

        if(categories[name] === undefined)
        {
          categories[name] = route
        }
        else
        {
          if(categories[name].length < route.length)
            categories[name] = route
        }
      }
      
      for (var key in categories)
      {
        result.push(categories[key])
      }

      if(result.length > 0)
        return <Text style ={{fontSize: 13, alignSelf: 'flex-end', color: Colors.StrongFocus}}>{result.join(' / ')}</Text>
    }
  }

  render(){
    return(
    <View style = {CategorySectionStyles.MainView}>
        <Text style ={{fontSize: 18, alignSelf: 'flex-end', color: Colors.WeakFocus}}>{this.props.entity.title}</Text>
        {this.renderCategoryPart()}
    </View>
    )}
  }