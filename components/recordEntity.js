import React from 'react';
import {View, TouchableOpacity,Platform, Image, Text , AsyncStorage} from 'react-native';
import Swiper from 'react-native-swiper';
import {Menu, MenuOptions, MenuTrigger, renderers, withMenuContext} from 'react-native-popup-menu';
import {withNavigation} from 'react-navigation';

import {GoodsRepository, ProfilesRepository, CommentRepository, LikeRepository} from '../repository';
// Styles
import MainStyles, { Colors, TranslateToPersianNumbers, ToJalali , TranslatePropertyName} from '../mainStyles';
import CategorySection from './sections/categorySection';
import {navigationReference, Context} from '../App'
import Button from './base/button';
import Input from './base/input';
import {CachedImage} from '../network';


export var selectedEntity = '';
var onDeleteReference = '';

const renderOptions = (props) => (
  <Menu  
    renderer={renderers.Popover} rendererProps={{placement: 'auto', preferredPlacement: 'bottom'}}>
      <MenuTrigger>
        <View style ={{width: '100%', height: '100%', justifyContent: 'center'}}>
          <Image resizeMode ='contain' style={{tintColor: Colors.VeryWeakFocus, height: '50%',borderWidth: 1,}} source={require('../resources/Icons/ic_action_more_vert.png')} />
        </View>
      </MenuTrigger>
      <MenuOptions>
          <View style ={{ padding: 0}}>
              <Button style ={{ borderRadius: 0, borderBottomWidth: 0.3, backgroundColor: Colors.WeakBack}} textPadding ={10} 
                onPress ={() =>{ 
                  selectedEntity = props.entity;
                  props.ctx.menuActions.closeMenu();
                  navigationReference.navigate('edit');
                }} 
                title="ویرایش"/>
              <Button style ={{borderRadius: 0, borderBottomWidth: 0.3,backgroundColor: Colors.WeakBack}} textPadding ={10} onPress ={() => null} title ="..."/>
              <Button style ={{borderRadius: 0, borderBottomWidth: 0.3, backgroundColor: Colors.WeakBack}} textPadding ={10} onPress ={() => null} title= "..."/>
              <Button textColor = {Colors.WeakAlarm} style ={{borderRadius: 0, borderBottomWidth: 0.3,backgroundColor: Colors.WeakBack}} textPadding ={10} 
                onPress ={() => {
                  GoodsRepository.deleteGood(props.entity.id).then(()=> {onDeleteReference()})
                  }} title="حذف"/>
          </View>
      </MenuOptions>
  </Menu> 
)

const renderEditButton = (props) => (
  <TouchableOpacity style={{marginLeft: 5, justifyContent: 'center'}} onPress = {() =>{
    // close open menus
    selectedEntity = props.entity;
    props.ctx.menuActions.closeMenu()

   navigationReference.navigate("edit")
   }}>
    <Image style ={{tintColor: Colors.WeakFocus, width:"68%", height:"68%", alignSelf: 'center'}} resizeMode = 'contain' source={require('../resources/Icons/ic_action_edit.png')}></Image>
  </TouchableOpacity>
)

const OptionsContext = withMenuContext(renderOptions);

const EditMenuContext = withMenuContext(renderEditButton);

export default class RecordEntity extends React.Component {
  constructor(props) {
    super(props);

    this.state ={
      purchaseMode: false
    }

    onDeleteReference = this.props.onDelete
    this.togglePurchaseMode = this.togglePurchaseMode.bind(this)
  }

  togglePurchaseMode()
  {
    this.setState({purchaseMode: !this.state.purchaseMode})
  }

  DoesImagesExist(){
    var images = JSON.parse(this.props.entity.pictures)
    if(!(images === undefined || images === null || images == null || images == undefined || images == "{}" || images == "[]"))
      if(images.length > 0)
        return true
      else
        return false
    else
      return false
  }

  renderImages()
  {
    var images = JSON.parse(this.props.entity.pictures)

    return images.map((image, index) =>
      <View key ={index} style = {MainStyles.RecentPageRecordSlideView}>
        <CachedImage viewStyle={{width: '100%', height: '100%'}} resizeMode = 'cover' style={{alignSelf: 'center', flex: 1, width: '100%', height: '100%'}} url = {image.uri}/>
      </View>
    )
  }

  render()
    {
    return (
      <Context.Consumer>
        {data => <View style = { MainStyles.RecentPageRecordEntityView}>
        <RecordInfoBar profile={data.profileData}  entity = {this.props.entity} showOptions = {this.props.showOptions}/>
        {this.DoesImagesExist()? (
        <Swiper dotStyle = {{width: 5, height: 5}} activeDotColor ='#fdb54e' paginationStyle={{ bottom: 0 }} style = {MainStyles.RecentPageRecordSwipperView}>
         {this.renderImages()}
        </Swiper>):(null)}
        <RecordDetails 
          purchases = {data.purchases} 
          updatePurchases = {data.updatePurchases}
          purchaseMode = {this.state.purchaseMode} 
          entity = {this.props.entity}/>
        <RecordActivityBar 
          purchases = {data.purchases}
          updatePurchases = {data.updatePurchases}
          togglePurchase = {this.togglePurchaseMode}
          purchaseMode = {this.state.purchaseMode} 
          profileData = {data} 
          entity = {this.props.entity}/>
        <RecordCommentSection entity = {this.props.entity}/>
      </View>}
      </Context.Consumer>
    );
  }
}

export class RecordCommentSection extends React.PureComponent { 
constructor(props){
  super(props)

  this.state = {
    comments: []
  }
}

componentDidMount()
{
  if(this.props.entity.comments === undefined || this.props.entity.comments == null)
  return null

  this.props.entity.comments.forEach((item) => {
    if(item.message == '-')
      return
    ProfilesRepository.getProfile(item.owner).then(profile => {
      this.state.comments.push({name: profile.firstname, message: item.message})
      this.setState({comments: this.state.comments})
      this.forceUpdate()
    })
  })
}

renderStatisticsComment(){
  likeCount = this.props.entity.likedislikes === undefined ? 0 : this.props.entity.likedislikes.length
  comments = this.props.entity.comments === undefined ? [] : this.props.entity.comments
  comments = comments.filter(x=> {return x.message != '-'})
  commentsCount = comments.length
  return <View key ={'statistics'} style ={MainStyles.RecordEntityCommentView}>
      <Text style ={ MainStyles.RecordEntityStatisticComment}>{TranslateToPersianNumbers(likeCount.toString())} نظر. {TranslateToPersianNumbers(commentsCount.toString())} پیام</Text>
  </View>
}

renderComments()
{
  return this.state.comments.map((item, index) =>
    <View key ={index} style ={MainStyles.RecordEntityCommentView}>
        <Text style ={ MainStyles.RecordEntityCommentUser}>{item.name}</Text>
        <Text style = { MainStyles.RecordEntityCommentText}>{item.message}</Text>
    </View>
  )
}

  render()
  {
    return <View style ={MainStyles.RecordEntityCommentSectionView}>
      {this.renderStatisticsComment()}
      {this.renderComments()}
    </View>
  }
}


export class RecordDetails extends React.Component { 
  constructor(props){
    super(props)

    this.state = {
      variantEnds: [],
      variants: [],
    }
  }

  setupVariants()
  {
    if(this.hasVariant())
    {
      this.state.variants = [];
      for(i = 0; i < this.props.entity.variants.length; i++)
      {
        var variant = this.props.entity.variants[i]
        var json = JSON.parse(variant.routes)

        var collection = {}
        this.jsonGauger(json, '', collection, false)

        for(var route in collection)
        {
          this.state.variantEnds.push({id: variant.id, end: route.split('|'), properties: collection[route]})
          statement = route + ': '
          for(var property in collection[route])
          {
            statement += '(' +
              TranslatePropertyName(property) +
              ':' + TranslateToPersianNumbers(collection[route][property].toString()) + ')'
          }

          this.state.variants.push(statement)
        }
      }

      this.setState({variants: this.state.variants, variantEnds: this.state.variantEnds})
    }
  }

  componentDidMount()
  {
    this.setupVariants();
  }

  componentWillReceiveProps()
  {
    this.setupVariants();
  }

  renderPart(label, value, isNumber){
    if(value == null || value == undefined || value === undefined)
      return null

    if(isNumber)
    { 
      if(value <= 0)
        return null
      value = TranslateToPersianNumbers(value.toString())
    }
    else
    {
      if(value == '' || value == '-')
        return null
    }

    return <View style ={MainStyles.RecordEntityDetailsPartChildView}>
      <Text style ={MainStyles.RecordEntityDetailsLabel}>{label}</Text>
      <Text style ={MainStyles.RecordEntityDetailsValue}>{value}</Text>
    </View>
  }

  jsonGauger (json, path, collection, passArray)
  {
    for(var x in json) {                    
      if(Array.isArray(json[x])) {
        this.jsonGauger(json[x], path+= x + '|', collection, true)
      } else {
        if (passArray){
          this.jsonGauger(json[x], path, collection, false)
        }
        else{
          collection[path.slice(0, -1)] = json
        }
      }
    }
  }

  hasVariant (){
    if(!(this.props.entity.variants == null || this.props.entity.variants === undefined))
    {
      if(this.props.entity.variants.length > 0)
      {
        return true;
      }
    }
    
    return false
  }

  renderPurchaseCount (end)
  {
    var filtered = this.props.purchases.filter(x =>{
      return (x.goodid == this.props.entity.id && x.variantid == end.id && x.variantend == end.end) 
    })

    if(filtered.length > 0)
    {
      return filtered[0].count
    }
    else
    {
      return 0
    }
  }

  handlePurchase(end, isPlus){
    var found = false
    var tobedeletedindex = -1
    this.props.purchases.forEach((x, index) =>{
      if(x.goodid == this.props.entity.id && x.variantid == end.id && x.variantend == end.end)
      {
        found = true
        x.count = (isPlus ? x.count +1 : x.count -1)

        if(x.count <= 0)
        {
          x.count = 0
          tobedeletedindex = index
        }

      }
    }, end, isPlus, found, tobedeletedindex)

    if(found == false)
    {
      if(!isNaN(this.props.entity.weight) && isPlus)
        this.props.purchases.push({good: this.props.entity, goodid: this.props.entity.id, goodowner: this.props.entity.owner,variantid: end.id, properties: end.properties, variantend: end.end, count: 1})
    }

    if(tobedeletedindex >= 0)
    {
      this.props.purchases.splice(tobedeletedindex, 1)
    }

    this.props.updatePurchases(this.props.purchases)
  }

  renderVariants(){
      return this.state.variants.map((statement, index) =>
      <View key ={index}>
          {index == 0 ? <Text style ={{padding: 5, fontSize: 15, fontWeight: 'bold'}}>گونه ها</Text> : null}
          <View style ={{alignItems : 'center', flexDirection: 'row-reverse'}}>
            {this.props.purchaseMode ? <View style ={{flexDirection : 'row-reverse', alignItems: 'center'}} >
              <Button 
                onPress ={() => this.handlePurchase(this.state.variantEnds[index], true)}
                title = "+" 
                style ={{margin: 5, width: 30, height: 30}}></Button>
              <Text>{this.renderPurchaseCount(this.state.variantEnds[index])}</Text>
              <Button 
              onPress ={() => this.handlePurchase(this.state.variantEnds[index], false)}
              title = "-" 
              style ={{margin: 5,width: 30, height: 30}}></Button>
              </View> : null}
            <Text>{statement.split('|').join(' ')}</Text>
          </View>
      </View>)
  }

  renderTags(label)
  {
    result = ''
    if(!(this.props.entity.hascategories === undefined))
    {
      tags = []
      for (i = 0; i < this.props.entity.hascategories.length; i++) {
        category = this.props.entity.hascategories[i]
        name = category.categorynameref
        route = category.categoryrouteref

        if(route == name)
          tags.push(name)
      }

      return tags.map((x, index)=> <View key ={index} style ={[MainStyles.RecordEntityDetailsPartChildView, {backgroundColor: Colors.WeakGold, borderRadius: 8, margin: 5, padding: 5}]}>
      <Text style ={MainStyles.RecordEntityDetailsValue}>{x}</Text>
    </View>)
    }
  }

  render() {
    return <View style ={MainStyles.RecordEntityDetailsView}>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
          {this.renderTags()}
        </View>
      </View>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
          {this.renderPart('کد', this.props.entity.code, false)}
          {this.props.entity.available==1?(
          this.renderPart('موجود','',false)):(null)}
        </View>
        <View style ={MainStyles.RecordEntityDetailsPartLeftChildren}>
        {this.renderPart('اجرت', this.props.entity.fee, true)}
        </View>
      </View>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
            {this.renderPart('شرکت', this.props.entity.company, false)}
            {this.renderPart('رنگ بندی', this.props.entity.color, false)}
        </View>
        <View style ={MainStyles.RecordEntityDetailsPartLeftChildren}>
          {this.renderPart('وزن', this.props.entity.weight, true)}
        </View>
      </View>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
          {this.renderPart('تاریخ اضافه شده', ToJalali(this.props.entity.createddate) , true)}
        </View>
        <View style ={MainStyles.RecordEntityDetailsPartLeftChildren}>
          {this.renderPart('سایز', this.props.entity.size, true)}
        </View>
      </View>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
          {this.renderPart('تاریخ تغییر داده شده', ToJalali(this.props.entity.modifieddate) , true)}
        </View>
      </View>
      <View style ={MainStyles.RecordEntityDetailsPartView}>
        <View style ={MainStyles.RecordEntityDetailsPartRightChildren}>
        </View>
      </View>
      {this.hasVariant()?
      <View style ={{margin: 10, padding: 5, borderWidth: .7, borderColor: Colors.Gold}}>
        {this.renderVariants()}
      </View>:null}

    </View>
  }
}


export class RecordInfoBar extends React.PureComponent {
  render() {
    return(
      <View style = {{padding: 3, height: 60, flexDirection: 'row', justifyContent: 'space-between', borderBottomWidth: 0.2, borderColor: Colors.StrongFocus,}}>
        <View style = {{paddingLeft: 3 , padding: 3, flex: 1,justifyContent: 'center', alignContent: 'center'}}>
          {(this.props.profile.owner == this.props.entity.owner) || this.props.profile.owner == 3 ?
             (this.props.showOptions == true ? <OptionsContext entity = {this.props.entity}/> : <EditMenuContext entity = {this.props.entity}/>):
             (null)}
        </View>
        <View style ={{padding: 8,flex: 5.5,justifyContent: 'center', alignContent: 'center'}}>
          <CategorySection entity = {this.props.entity} ></CategorySection>
        </View>
        <View style = {{paddingLeft: 3 , padding: 3, flex: 1,justifyContent: 'center', alignContent: 'center'}}>
          <Image style ={{tintColor: Colors.WeakGold,width: '90%', height: '90%'}} resizeMode = 'contain' source = {require('../resources/Icons/ic_action_tag.png')}/>
        </View>
      </View>
    )
  }
}

export class RecordActivityBar extends React.Component {
  constructor(props)
  {
    super(props)

    this.state={
      messageToggle: false,
      message : '',
      beforeEditing: '',
      islike: 2,
    }
  }

  componentDidMount()
  {
    likes = this.props.entity.likedislikes === undefined ? [] : this.props.entity.likedislikes
    ownLikes = likes.filter(x=> {
      return x.owner == this.props.profileData.profileData.owner
    })

    isLiked = ownLikes.length > 0 ? ownLikes[0].islike : 2
    this.setState({islike: isLiked})
  }

  renderColorLike(){

    if(this.state.islike == 1)
      return Colors.WeakGold
    else  
      return Colors.VeryWeakFocus
  }

  handleLike(input) {
    goodid = this.props.entity.id
    owner = this.props.profileData.profileData.owner
    isLiked = this.state.islike

    if(input == 1)
    {
      if(isLiked == 1)
      {
        this.handleLikeFeedback(2)
        LikeRepository.PutLikeDislike(goodid, owner, 2).then((model) =>{
          if(model.islike == 2)
          {
            this.handleLikeFeedback(2)
          }
        })
      }
      if(isLiked == 2 || isLiked == 0)
      {
        this.handleLikeFeedback(1)
        LikeRepository.PutLikeDislike(goodid, owner, 1).then((model) =>{
          if(model.islike == 1)
          {
            this.handleLikeFeedback(1)
          }
        })
      }
    }
    
    if(input == 0)
    {
      if(isLiked == 0)
      {
        this.handleLikeFeedback(2)
        LikeRepository.PutLikeDislike(goodid, owner, 2).then((model) =>{
          if(model.islike == 2)
          {
            this.handleLikeFeedback(2)
          }
        })
      }
      if(isLiked == 1 || isLiked == 2)
      {
        this.handleLikeFeedback(0)
        LikeRepository.PutLikeDislike(goodid, owner, 0).then((model) =>{
          if(model.islike == 0)
          {
            this.handleLikeFeedback(0)
          }
        })
      }
    }
  }

  handleLikeFeedback(islike)
  {
    this.setState({islike: islike})
    
  }

  toggleMessage()
  {
    this.state.messageToggle = !this.state.messageToggle

    if(this.state.messageToggle)
    {
      messages = this.props.entity.comments === undefined ? [] : this.props.entity.comments
      ownComments = messages.filter(x=> {
        return x.owner == this.props.profileData.profileData.owner
      })

      this.state.beforeEditing = ownComments.length > 0 ? ownComments[0].message : ''
      this.state.message = ownComments.length > 0 ? ownComments[0].message : ''

      if(this.state.message == '-' || this.state.beforeEditing == '-')
      {
        this.state.message = ''
        this.state.beforeEditing = ''
      }
    }

    this.setState({ messageToggle: this.state.messageToggle,
       message: this.state.message,
       beforeEditing: this.state.beforeEditing
      })

    this.forceUpdate()
  }

  renderColordisLike(){
    if(this.state.islike == 0)
      return Colors.WeakAlarm
    else  
      return Colors.VeryWeakFocus
  }

  renderColorMessage(){
    if(this.state.messageToggle)
      return Colors.Submit
    else
      return Colors.VeryWeakFocus
  }

  renderInputMessage ()
  {
    if(this.state.messageToggle)
    {
      return <View style ={{ margin: 20}}>
          <Input 
            onChangeHandler = {(text) => {
              this.setState({message: text})
            }}
            onEndEditing = {(text) => {
             if(this.state.message != this.state.beforeEditing)
              {
                CommentRepository.putComment({goodid: this.props.entity.id, owner: this.props.profileData.profileData.owner, message: this.state.message}).then(()=> this.setState({messageToggle: false}))
              }
            }} 
            value = {this.state.message} placeHolder ='تایپ کنید...' style ={{}}></Input>
        </View>
    }
    else
      return null
  }

  renderPurchaseColor ()
  {
    if(this.props.purchaseMode)
      return Colors.Submit
    else 
      return Colors.VeryWeakFocus
  }

  handlePurchase(isPlus){
    var found = false
    var tobedeletedindex = -1
    this.props.purchases.forEach((x, index) =>{
      if(x.goodid == this.props.entity.id && x.variantid == null)
      {
        found = true

        x.count = (isPlus ? x.count +1 : x.count -1)

        if(x.count <= 0)
        {
          x.count = 0
          tobedeletedindex =index
        }

      }
    }, isPlus, found, tobedeletedindex)

    if(found == false)
    {
      if(!isNaN(this.props.entity.weight) && isPlus)
        this.props.purchases.push({good: this.props.entity, goodid: this.props.entity.id, goodowner: this.props.entity.owner, variantid: null, count: 1})
    }

    if(tobedeletedindex >= 0)
    {
      this.props.purchases.splice(tobedeletedindex, 1)
    }

    this.props.updatePurchases(this.props.purchases)
  }

  renderPurchaseCount ()
  {
    //updatePurchases
    var filtered = this.props.purchases.filter(x =>{
      return (x.goodid == this.props.entity.id && x.variantid == null)
    })

    if(filtered.length > 0)
    {
      return filtered[0].count
    }
    else
    {
      return 0
    }
  }

  render(){
    return (
      <View>
      <View style = {MainStyles.RecentPageRecordActivityBarView}>
        <View style = {{alignSelf: 'flex-start', flexDirection: 'row'}}>
        {(this.props.entity.owner == this.props.profileData.profileData.owner)?(null):(        <TouchableOpacity onPress ={ () =>{
          this.props.togglePurchase()
        }}>
          <Image fadeDuration={0} style ={[MainStyles.activityButtons, {tintColor: this.renderPurchaseColor()}]} source={require('../resources/Icons/activity_bar_purchase.png')} />
        </TouchableOpacity>)}
        {this.props.purchaseMode ? <View style ={{flexDirection : 'row-reverse', alignItems: 'center'}} >
              <Button 
                onPress ={() => this.handlePurchase(true)}
                title = "+" 
                style ={{marginLeft: 5,marginRight: 20, width: 30, height: 30}}></Button>
              <Text>{this.renderPurchaseCount()}</Text>
              <Button
                onPress ={() => this.handlePurchase(false)}
                title = "-" style ={{marginLeft: 20, marginRight: 5,width: 30, height: 30}}></Button>
              </View> : null}
        {/* <TouchableOpacity>
          <Image fadeDuration={0} style ={[MainStyles.activityButtons, {tintColor: Colors.Gold}]} source={require('../resources/Icons/activity_bar_favorite_off.png')} />
          </TouchableOpacity> */}
        </View>
        <View style = {{alignSelf: 'flex-start', flexDirection: 'row-reverse'}}>
          <TouchableOpacity onPress ={() => this.handleLike(1)}>
            <Image fadeDuration={0} style ={[MainStyles.activityButtons, {tintColor: this.renderColorLike()}]} source={require('../resources/Icons/activity_bar_thumbs_up.png')} />
          </TouchableOpacity>
          <TouchableOpacity onPress ={() => this.handleLike(0)}>
            <Image fadeDuration={0} style ={[MainStyles.activityButtons, {tintColor: this.renderColordisLike()}]} source={require('../resources/Icons/activity_bar_thumbs_down.png')} />
          </TouchableOpacity>
          <TouchableOpacity onPress ={() =>{this.toggleMessage() }}>
            <Image fadeDuration={0} style ={[MainStyles.activityButtons, {tintColor: this.renderColorMessage()}]} source={require('../resources/Icons/activity_bar_comment.png')} />
          </TouchableOpacity>
        </View>
      </View>
      {this.renderInputMessage()}
      </View>
    );
  }
}