import React from 'react';
import {View, TouchableOpacity, Image, Text, Button, AsyncStorage} from 'react-native';
import {CachedImage} from '../network';

import {ProfilesRepository} from '../repository';

// Styles
import MainStyles, {Colors, DrawerStyles, TranslateToPersianNumbers} from '../mainStyles';
import { Context, navigationReference } from '../App';
  
export default class ProfileSection extends React.Component {
  
  componentDidMount() {
    this.loadProfile()
  }
  
  loadProfile()
  {
    AsyncStorage.getItem("userId")
    .then((userId) => {
      if(userId != null) {
        ProfilesRepository.getProfile(userId).then((profile)=>{ 
          {this.props.loadProfile(profile)}
        })
      }
    })
  }

  render(){
    return(
      <Context.Consumer>
        {data =>  <View style ={DrawerStyles.ProfileSectionView}>
      <View style = {DrawerStyles.ProfileSectionTopView}>
        <View style = {DrawerStyles.ProfileSectionPersonDetailsView}>
          <View style ={DrawerStyles.ProfielSectionPersonDetailsName}>
            <Text>{data.profileData.firstname + ' ' + data.profileData.middlename + ' ' + data.profileData.lastname}</Text>
            <Text>{TranslateToPersianNumbers(data.profileData.phonenumber)}</Text>
          </View>
        </View>
        <TouchableOpacity onPress ={() => {
          navigationReference.navigate('user', {profile: data.profileData, currentProfile: data.profileData})
        }} style = {DrawerStyles.ProfileSectionAvatarView}>
          <Image resizeMode = 'cover' style = {DrawerStyles.ProfileSectionAvatar} source={{uri: data.profileData.profilepictureurl}}></Image>
        </TouchableOpacity>
      </View>
      <View style = {DrawerStyles.ProfileSectionBotView}>
        <Text>{data.profileData.companynumber}</Text>
      </View>
      {this.props.isAdmin? null:<Text style ={{color: Colors.Gold}}>محدودیت وزنی : {data.profileData.weight == null ? 0 : data.profileData.weight}</Text>}
    </View>}
      </Context.Consumer>
    )}
  }