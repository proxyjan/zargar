import React from 'react';
import {View, Image, FlatList,Text, ActivityIndicator, TouchableNativeFeedback, RefreshControl, TouchableOpacity} from 'react-native';

import MainStyles, { Colors , PeoplePageStyles, TranslateToPersianNumbers} from '../../mainStyles';
import Input from '../base/input';
import {ProfilesRepository} from '../../repository';
import {CachedImage} from '../../network';
import Button from '../base/button';
import { Context ,navigationReference} from '../../App'

export default class PeoplePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
      refreshing: false,
      selected: "",
      selectedIndex: null
    };

    this.handleSearch = this.handleSearch.bind(this)
  }

  static navigationOptions = {
    tabBarLabel: 'مردم',
    tabBarIcon: ({tintColor}) => (
      <Image source = {require('../../resources/Icons/tab_bar_people.png')} style = {[MainStyles.tabBarIcon, {tintColor: tintColor}]}/>
    )}

    componentDidMount() {
      this.makeRemoteRequest();

    }
  
    makeRemoteRequest = () => {
      this.setState({ loading: true });
      ProfilesRepository.getProfiles().then(profiles => {
        this.setState({
          data: profiles,
          filteredData: [],
          loading: false,
          refreshing: false
        }, () => 
        this.handleSearch());
      })
    }
  
    handleRefresh = () => {
      this.setState(
        {
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };

  
    renderSeparator = () => {
      return (
        <View
          style={{
            height: 5
          }}
        />
      );
    };

    handleSearch (text) 
    {
      filteredData = this.state.data.filter(x => {
        if(text == '' || text == null)
          return true;

        firstnamecondition = false
        lastnamecondition = false
        mobilenumbercondition = false

        if(x.firstname != null)
        {
          if(x.firstname != '')
          {
            firstnamecondition = x.firstname.includes(text)
          }
        }

        if(x.lastname != null)
        {
          if(x.lastname != '')
          {
            lastnamecondition = x.lastname.includes(text)
          }
        }

        if(x.mobilenumber != null)
        {
          if(x.mobilenumber != '')
          {
            mobilenumbercondition = x.mobilenumber.includes(text)
          }
        }

        return (firstnamecondition || lastnamecondition || mobilenumbercondition)
      })

      this.setState({filteredData: filteredData})
    }

    renderFooter = () => {
      if (!this.state.loading)
      {
        return <View style={{height: 170}}/>;
      }
      else
      {
        return <View
            style={{
              height: 170,
              paddingVertical: 20,
              borderTopWidth: 0.3,
            }}
          >
            <ActivityIndicator color= {Colors.Gold} animating size="large" />
          </View>
      }
    };

    select = (item, index) => 
    {
      this.setState({selected : item.mobilenumber, selectedIndex: index});
    };

    renderItem = (item, index, currrentProfileData) => 
    {
      if(this.state.selected == item.mobilenumber)
      {
        return(
          <View>
          <View style = {PeoplePageStyles.SelectedItemView}>
          <TouchableOpacity onPress ={() => {
            navigationReference.navigate('user', {profile: item, currentProfile: currrentProfileData})
          }} style ={{margin: 5, height: 150, width: 150, borderRadius: 120, overflow:'hidden'}}>
            <CachedImage resizeMode = 'cover' style={{alignSelf: 'center', width: 150, height: 150}} viewStyle={{height: 150, width:150}} url =  {item.profilepictureurl}/>
          </TouchableOpacity>
            <View style ={{padding: 10, flex: 2, flexDirection: 'column', alignContent: 'center', justifyContent: 'space-between'}}>
              <Text  style = {{fontSize: 18, color: Colors.StrongFocus}}>{item.firstname} {item.lastname}</Text>
              <Text  style = {{fontSize: 13, color: Colors.StrongFocus}}>{item.companynumber}</Text>
              <Text  style = {{fontSize: 13, color: Colors.StrongFocus}}>{item.address}</Text>
              <Text style = {{fontSize: 20, color: Colors.WeakFocus}}>{TranslateToPersianNumbers(item.mobilenumber)}</Text>
              <View style = {{flexDirection: 'row', padding: 0}}> 
              </View>
            </View>
          </View>
        </View>
          
        )
      }
      else{
        return(
        <TouchableNativeFeedback onPress={() => {this.select(item, index)}}>
        <View style={{alignSelf: 'center',shadowColor: Colors.WeakFocus,
      shadowOffset: { width: 4, height: 2 },
      shadowOpacity: 0.9,
      shadowRadius: 3,
      elevation: 1, borderRadius: 5, alignContent: 'center', backgroundColor: Colors.WeakBack, flexDirection: 'row-reverse', justifyContent: 'space-between', height: 85, width : '97%'}}>
          <View style ={{margin: 5, height: 70, width: 70, borderRadius: 70, overflow:'hidden'}}><CachedImage resizeMode = 'cover' style={{alignSelf: 'center', width: 70, height: 70}} viewStyle={{height: 70, width:70}} url =  {item.profilepictureurl}></CachedImage></View>
          <View style ={{padding: 15, flex: 3, flexDirection: 'column', alignContent: 'center', justifyContent: 'space-evenly'}}>
            <Text style = {{fontSize: 18}}>{item.firstname} {item.lastname}</Text>
            <Text style = {{alignSelf: 'flex-end', direction: 'rtl', fontSize: 18}}>{TranslateToPersianNumbers(item.mobilenumber)}</Text>
          </View>
        </View>
      </TouchableNativeFeedback>
      )}
    };

    render() {
        return (
          <Context.Consumer>
          {data => <View style={{height: '100%', backgroundColor: Colors.Back }}>
            <View style ={{height: 70, backgroundColor: Colors.Back}}>
              <Input style= {{alignSelf: 'center', width: '95%'}} onChangeHandler = {this.handleSearch} placeHolder="نام, نام خانوادگی یا شماره ..." />
            </View>
            <FlatList
              extraData={this.state.selected}
              data={this.state.filteredData}
              renderItem={({ item, index }) => (this.renderItem(item, index, data.profileData))}
              keyExtractor={item => item.owner.toString()}
              ItemSeparatorComponent={this.renderSeparator}
              ListFooterComponent={this.renderFooter}
              refreshControl={
                <RefreshControl
                    refreshing={this.state.refreshing}
                    onRefresh={this.handleRefresh}
                    colors ={[Colors.WeakFocus]}
                    progressBackgroundColor = {Colors.WeakGold}
                 />
              }
            />
          </View>}
          </Context.Consumer>
        );
      }
  }

