import React from 'react';
import {View, Text, AsyncStorage, ActivityIndicator} from 'react-native';
import Input from '../base/input';
import Button from '../base/button';

import {ProfilesRepository} from '../../repository';
import {navigationReference, apiLink} from '../../App';
import {token} from '../../repository';
import App, { Context } from '../../App';
import { Colors } from '../../mainStyles';

var RNFetchBlob = require('rn-fetch-blob').default;


export default class LoginPage extends React.Component{

    render()
    {
        return <Context.Consumer>
            {data => <LoginSection loadProfile = {data.loadProfile}/>}
        </Context.Consumer>
    }
}

export class LoginSection extends React.Component {
    constructor() {
        super();
        this.state = {
            username: '',
            password: '',
            trying: false,
            failed: false,
        };
    }

    fethProfile()
    {
      AsyncStorage.getItem("userId")
      .then((userId) => {
        if(userId != null) {
            ProfilesRepository.getProfile(userId).then((profile)=>{ 
                {this.props.loadProfile(profile)}
            })
        }
      })
    }

    onUsernameChange = (text) =>
    {
        this.setState({username: text})
    }

    onPasswordChange = (text) =>
    {
        this.setState({password: text})
    }

    createAccount (){
        this.setState({trying: true, failed: false});

        bodyJson = JSON.stringify({
            email: this.state.username,
            password: this.state.password,
            })

            fetch(apiLink + 'Users', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                body: bodyJson
            })
            .then((responseJson) => {
              responseJson.json().then((body) => {
                  if(body.id === undefined)
                  {
                    this.setState({failed: true, trying: false});
                  }
                  else
                  {
                    ProfilesRepository.create(body.id).then(() => {
                        this.onLogin()
                    })
                    .catch((e) => {
                        console.log(e)
                    })
                  }
              });
            })
            .catch((error) => {
              console.error(error);
            });
    }

    onLogin = () => 
    {
        this.setState({trying: true, failed: false});

        bodyJson = JSON.stringify({
            email: this.state.username,
            password: this.state.password,
            })

        fetch(apiLink + 'Users/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: bodyJson
        })
        .then((responseJson) => {
          responseJson.json().then((body) => {
              if(body.id === undefined)
              {
                this.setState({failed: true, trying: false});
              }
              else
              {
                this.setState({failed: false, trying: false})
                AsyncStorage.setItem("token", body.id).then(()=>{
                    AsyncStorage.setItem("userId", body.userId.toString()).then(()=>{
                        this.fethProfile()
                        navigationReference.navigate('main')
                    })
                })
              }
          });
        })
        .catch((error) => {
          console.error(error);
        });
    }

    render() 
    {
        return <View style ={{width: '100%', height: '100%', justifyContent: 'center'}}>
            <View style ={{width: '50%', height: 200, flexDirection: 'column', justifyContent: 'flex-start', alignContent:'center', alignSelf: 'center' }}>
                {this.state.failed ? (<Text style={{alignSelf: 'center', color: 'red'}}>خطا در ورود... دوباره تلاش کنید!</Text>) : (null)}
                <Input keyboardType = 'email-address' onChangeHandler ={this.onUsernameChange} placeHolder = "email" style={{height: 60, textAlign:'left',}}></Input>
                <Input secureTextEntry = {true} onChangeHandler ={this.onPasswordChange} placeHolder = "password" style ={{height: 60, textAlign:'left'}}></Input>
                {this.state.trying ? (<ActivityIndicator animating={true} color ={Colors.Gold} style ={{marginTop: 10, height: 30,opacity : 1}} />) : ( 
                    <View><Button onPress = {() => this.onLogin()} style ={{marginTop: 10, height: 30}} title="ورود"></Button>
                    <Button onPress = {() => this.createAccount()} style ={{marginTop: 10, height: 30}} title="ساخت حساب"></Button></View>)}
            </View>
        </View>
    }
}
