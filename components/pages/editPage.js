import React from 'react';
import {Text, Alert, Dimensions, View, Platform, Image, ImageBackground,  Slider, Picker, ScrollView, StatusBar, Switch, ActivityIndicator} from 'react-native';
import { TabView, TabBar, SceneMap, PagerAndroid , PagerExperimental  } from 'react-native-tab-view';

import MainStyles, {Colors, EditPageStyles, TranslatePropertyName} from '../../mainStyles';
import Button from '../base/button';
import Input from '../base/input';
import Check from '../base/check';
import ActionButton from '../base/actionButton';
import {navigationReference} from '../../App';
import ImagePicker from 'react-native-image-crop-picker';
import { CachedImage } from '../../network';
import CategoryHeirarchy from '../categoryHeirarchy';
import _ from 'lodash';

import {selectedEntity} from '../recordEntity';
import Selector from '../base/selector';
import {GoodsRepository, VariantRepository, CategoryRepository} from '../../repository';
import * as GestureHandler from 'react-native-gesture-handler';

var RNFetchBlob = require('rn-fetch-blob').default;

const tabViewInitialLayout = {
  height: Dimensions.get('window').height,
  width: Dimensions.get('window').width,
};

export class AttachmentSection extends React.PureComponent {

  renderOnServerImages(){
    var images = JSON.parse(this.props.goodinfo.pictures)
    return images.map((image, index) =>
    <View key ={index} style ={{padding: 4, flexDirection: 'row', justifyContent: 'flex-end'}}>
      <CachedImage resizeMode = 'cover' style ={{margin: 5, width: '100%', height: 150, alignSelf: 'center'}} url ={ image.uri}/>
      <Button style ={{width: 40, height: 40, margin: 10, borderRadius: 15, alignSelf :'center', backgroundColor: Colors.WeakAlarm}} title = '-' onPress = {() => this.props.serverImageDeleter(index)}></Button>
    </View>
    )

  }

  renderToUploadImges () {
      return this.props.imagePaths.map((path, index) =>
        <View key ={index} style ={{padding: 4, flexDirection: 'row', justifyContent: 'flex-end'}}>
          <Image resizeMode = 'cover' style ={{margin: 5, width: this.props.uploadProgresses[index] === undefined ? '0%' : this.props.uploadProgresses[index].toString() + '%', height: 150, alignSelf: 'center'}} source ={{uri: path}}></Image>
          <Button style ={{width: 40, height: 40, margin: 10, borderRadius: 15, alignSelf :'center', backgroundColor: Colors.WeakGold}} title = '-' onPress = {() => this.props.imageDeleter(index)}></Button>
         </View>
         )
  }

  render() 
  {
    return <View style={[{padding: 10}, { backgroundColor: Colors.Back }]}>
      <ScrollView style ={EditPageStyles.InfoSectionView}>
      {this.renderOnServerImages()}
      <View style ={{height: 0.8, marginRight: 30, marginLeft: 30, margin: 10, backgroundColor: 'black'}}></View>
      {this.renderToUploadImges()}
      <Button style ={{width: 40, height: 40, margin: 10, borderRadius: 15, alignSelf :'center'}} title = '+' onPress = {() => this.props.uploader()}></Button>
      </ScrollView>
    </View>
  }
}

export class VariantSection extends React.Component {
  constructor(props){
    super(props)
      
      this.state = {
        variants : [],
        fakeid : 1,
        existingids : []
      }

  }

  serilizeRoutes(layer, routes, json)
  {
    // Create results based on routes
    results = {}
    remainingRoutes = []
    routes.forEach((route, index) =>{
      splitedRoute = route["route"].split('-')
      key = splitedRoute.slice(0,layer +1).join('-')

      value = ((splitedRoute.length == (layer + 1)) ? (route["values"]) : (null))
      
      if(!(key in results))
        results[key] = []

      if(value != null)
        results[key].push(value)

      if(layer + 1 < splitedRoute.length)
        remainingRoutes.push(route)

    }, layer)

    // Assamble the json object with results
    for(var key in results)
    {
      var splited = key.split('-')
      var letter = splited.pop()

      if(splited.length == 0)
      {
        json[letter] = results[key]
      }
      else
      {
        array = this.getArrayFromPath(json, splited)
        var ob = {}
        ob[letter] = results[key]
        array.push(ob)
      }

    }

    if(remainingRoutes.length > 0)
      this.serilizeRoutes(layer + 1, remainingRoutes , json)
  }

  getArrayFromPath(json, path)
  {
    if(path.length == 1)
    {
      return json[path]
    }
    else
    {
      return this.getArrayFromPathRecursive(json[path], path.shift())
    }
  }

  getArrayFromPathRecursive(array, path)
  {
    key = path[0]
    for(let i = 0; i < array.length; i++)
    {
      if(key in array[i])
      {
        if(path.length == 1)
        {
          return array[i][key]
        }
        else
        {
          return this.getArrayFromPathRecursive(array[i][key], path.shift())
        }
      }
    }
  }


  updateModel()
  {
    modelsToPost = []
    modelsToUpdate = []
    this.state.variants.forEach((variant, index) =>{

      var fields = variant.fields.join('-')
      if(fields.length == 0)
        return;

      var routes = {}
      this.serilizeRoutes(0, variant.routes, routes)
      routes = JSON.stringify(routes)

      if(variant.id < 0) {
        modelsToPost.push({fields: fields, routes: routes})
      }
      else {
        modelsToUpdate.push({id: variant.id, fields: fields, routes: routes})
      }
    }, modelsToPost, modelsToUpdate)

    VariantRepository.PostVariant(modelsToPost).then((x) => {
      if(Array.isArray(x))
      {
        x.forEach((response) => {
          var goodid = this.props.goodinfo.id
          var variantid = response.id
          var model = {goodid: goodid, variantid: variantid}
          VariantRepository.PostHasVariant(model).then((y)=>{
            console.log(y)
          })
        })
      }
      else
      {
        response = x
        var goodid = this.props.goodinfo.id
        var variantid = response.id
        var model = {goodid: goodid, variantid: variantid}
        VariantRepository.PostHasVariant(model).then((y)=>{
          console.log(y)
        })
      }
    })

    VariantRepository.updateVariants(modelsToUpdate).then((x) => {})

    modelsIds = modelsToUpdate.map(x => x.id)

    deletedids = this.state.existingids.filter(x=> {
      return !modelsIds.includes(x)
    })

    deletedids.forEach(x=>{
      VariantRepository.DeleteVariant(x).then()
    })
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  componentDidMount()
  {
    this.props.onRef(this)
    this.loadData()
  }

  hasVariant (){
    if(!(this.props.goodinfo.variants == null || this.props.goodinfo.variants === undefined))
      if(this.props.goodinfo.variants.length>0)
        return true;
    
    return false
  }

  jsonGauger (json, path, collection, passArray)
  {
    for(var x in json) {                    
      if(Array.isArray(json[x])) {
        this.jsonGauger(json[x], path += (path == ''? '' : '-') + x, collection, true)
      } 
      else {
        if (passArray){
          this.jsonGauger(json[x], path, collection, false)
        }
        else{
          collection[path] = json
        }
      }
    }
  }

  loadData()
  {
    if(this.hasVariant()){
      for(i = 0; i < this.props.goodinfo.variants.length; i++){
        var variant = this.props.goodinfo.variants[i]
        var routes = JSON.parse(variant.routes)
        var fields = variant.fields.split('-')
        var id = variant.id;
        this.state.existingids.push(id)

        var collection = {}
        this.jsonGauger(routes, '', collection, true)

        var newCollection = []
        for(var r in collection)
        {
          newCollection.push({route: r, values : collection[r]})
        }

        this.state.variants.push({id: id, fields: fields, routes: newCollection})

      }
      this.setState({variants:this.state.variants, existingids: this.state.existingids})
    }
  }

  selfRoutes(variant)
  {
    filtered = variant.routes.filter((x)=>
    {
      return (x.route == variant.routes[0].route.split('-')[0])
    }, variant)

    return filtered
  }

  createSelftoggle(variant)
  {
    var r = this.selfRoutes(variant)
    return <Check 
    title ='#'
    value = {r.length == 0 ? false : true}
    onPress = {() => {
      if(r.length > 0)
      {
        Alert.alert(
          'اخطار',
          'مقادیر وارد شده ی این سر دسته پاک خواهد شد. مطمن هستید؟',
          [
          {text: 'خیر', onPress: () => {

          }, style: 'cancel'},

          {text: 'بله', onPress: () => {
            variant.routes = variant.routes.filter((x)=>
            {
              return x.route != variant.routes[0].route.split('-')[0]
            },variant)

            if(variant.routes.length == 0)
            {
              var variantIndex = this.state.variants.indexOf(variant)
              this.state.variants.splice(variantIndex, 1)
            }

            // SAVE
            this.setState({variants: this.state.variants})

          }},],{ cancelable: false }
      )
      }
      else
      {
        // CHANGE FIELDS
        values = {}
        variant.fields.forEach((fieldName)=> {
          values[fieldName] = ''
        }, values, variant)

        variant.routes.push({route : variant.routes[0].route.split('-')[0], values: values})
      }

      // SAVE
      this.setState({variants: this.state.variants})
    }}
    ></Check>
  }

  createCheck(variant, name, title)
  {
    return <Check 
    value = {variant.fields.includes(name)? true : false} title={title}
    onPress = {() => {
      if(variant.fields.includes(name))
      {
        Alert.alert(
          'اخطار',
          'مقادیر وارد شده ی این مشخصه پاک خواهد شد. مطمن هستید؟',
          [
          {text: 'خیر', onPress: () => {

          }, style: 'cancel'},

          {text: 'بله', onPress: () => {
            index = variant.fields.indexOf(name)
            // CHANGE FIELDS
            variant.fields.splice(index, 1)
            // CHANGE VARIANT ROUTES
            variant.routes.forEach((route)=>{
              delete route.values[name]
            })

            // SAVE
            this.setState({variants: this.state.variants})

          }},],{ cancelable: false }
      )
      }
      else
      {
        // CHANGE FIELDS
        variant.fields.push(name)
        // CHANGE VARIANT ROUTES
        variant.routes.forEach((route)=>{
          route.values[name] = ''
        })
      }

      // SAVE
      this.setState({variants: this.state.variants})
    }}
    ></Check>
  }

  renderVariants ()
  {
    var result = []

    result.push(<View key ={'addvariant'} style ={EditPageStyles.AddVariantView}>
      <Button 
        style ={{width: 50, height: 50, borderRadius:20}} 
        title = '+'
        onPress ={() => {
          this.state.variants.unshift({id: -(this.state.fakeid++) , fields: [], routes: [{route: 'گونه ی جدید-مدل جدید', values : {}}]})
          this.setState({variants: this.state.variants})
        }}
        />
    </View>)

    result.push(<View key ={'seperater'} style ={{margin: 20, height: .5, backgroundColor: Colors.WeakFocus}}></View>)

    this.state.variants.forEach((variant)=>{

      // ---------- VARIANT NAME - TITLE OF THE VARIANT ---------
      result.push(<View key =  {'variant' + variant.id} style = {EditPageStyles.VariantSectionTitleView}>
      {this.createSelftoggle(variant)}
      <Input style={{}}
        value ={variant.routes[0].route.split('-')[0]}
        onChangeHandler ={(newText) => {
          variant.routes.forEach(x=>{
            splited = x.route.split('-')
            splited.shift()
            splited.unshift(newText)
            x.route = splited.join('-')
          })
          this.setState({variants: this.state.variants})
        }}
         />
         </View>)

        // ----------- FIELD SELECTION - BELOW THE TITLE ------------
        result.push(<View key = {variant.id + 'properties'} style = {EditPageStyles.VariantSectionTagView}>
            {this.createCheck(variant, 'size', 'سایز')}
            {this.createCheck(variant, 'color', 'رنگ')}
            {this.createCheck(variant, 'fee', 'اجرت')}
            {this.createCheck(variant, 'weight', 'وزن')}
        </View>)

        var exisitingSelfRoutes = this.selfRoutes(variant)
        if(exisitingSelfRoutes.length > 0 )
        {

          exisitingSelfRoutes.forEach((xselfroutes)=>{
            Object.keys(xselfroutes.values).forEach(function(p, propertyIndex) {
              result.push(<View key ={'variant' + variant.id + 'selfRoute' + propertyIndex + p} style ={EditPageStyles.VariantSectionPropertyView}>
                <Text style ={{fontWeight: 'bold'}}>{TranslatePropertyName(p)}</Text>
                <Input 
                  value = {xselfroutes.values[p].toString()}
                  onChangeHandler = {(newText) =>{
                    xselfroutes.values[p] = newText
                    this.setState({variants: this.state.variants});
                  }}
                ></Input>
              </View>)
            }, this)
          })
        }

        variant.routes.forEach(function(currentRoute, currentRouteIndex){
          // -------- EACH ROUTE INSIDE THE VARIANT - INPUT ------------

          if(currentRoute.route == variant.routes[0].route.split('-')[0])
            return;

          result.push(<View key ={'routes' + variant.id + currentRouteIndex} style ={EditPageStyles.VariantSectionRouteView}>
          <Input
            value = {currentRoute.route.split('-').slice(1).join('-')}
            onChangeHandler = {(newText) =>{
              var ff = currentRoute.route.split('-').splice(0,1);
              ff.push(newText);
              currentRoute.route = ff.join('-')
              this.setState({variants: this.state.variants});
            }}
          />
          <Button 
            style ={{margin: 20, width: 25, height: 25, backgroundColor: Colors.WeakAlarm}}
            title = '-' 
            onPress ={() =>{
              if(variant.routes.length == 1)
              {
                Alert.alert(
                  'اخطار',
                  'مطمین به پاک کردن این گونه هستید؟',
                  [
                  {text: 'خیر', onPress: () => {
        
                  }, style: 'cancel'},
        
                  {text: 'بله', onPress: () => {
  
                    variant.routes.splice(0,1)
                    var variantIndex = this.state.variants.indexOf(variant)
                    this.state.variants.splice(variantIndex, 1)
        
                    // SAVE
                    this.setState({variants: this.state.variants})
        
                  }},],{ cancelable: false }
              )
              }
              else
              {
                var ind = variant.routes.indexOf(currentRoute)
                variant.routes.splice(ind, 1)
                this.setState({variants: this.state.variants});
              }
            }}
            />
          </View>)

          // -------- PROPERTIES OF EACH ROUTE INSIDE THE VARIANT - INPUT ------------
          Object.keys(currentRoute.values).forEach(function(p, index) {
            result.push(<View key ={'variant' + variant.id + 'route' + currentRouteIndex + 'property' + index} style ={EditPageStyles.VariantSectionPropertyView}>
              <Text style ={{fontWeight: 'bold'}}>{TranslatePropertyName(p)}</Text>
              <Input 
                value = {currentRoute.values[p].toString()}
                onChangeHandler = {(newText) =>{
                  currentRoute.values[p] = newText
                  this.setState({variants: this.state.variants});
                }}
              ></Input>
            </View>)
          }, this)
        }, this, variant);

        result.push(<View key ={variant.id + 'addroute'} style ={EditPageStyles.AddRouteView}>
          <Button 
            title ={'+ ' + variant.routes[0].route.split('-')[0]}
            onPress ={() => {
              var newValues = {}
              Object.keys(variant.routes[0].values).forEach(function(key) {
                newValues[key] = ""
              }, newValues)
              variant.routes.push({route: variant.routes[0].route.split('-')[0] + '-' + 'مدل جدید' , values: newValues})
              this.setState({variants: this.state.variants});
            }}
            ></Button>
        </View>)

        result.push(<View key={variant.id + 's2'} style ={{margin: 20, height: .5, backgroundColor: Colors.WeakFocus}}></View>)
    },this)
    return result;
  }

  render() {
    return  <View style={[{padding: 10}, { backgroundColor: Colors.Back }]}>
      <ScrollView style ={EditPageStyles.InfoSectionView}>
        <Text style ={{paddingBottom: 20, alignSelf: 'center', fontStyle: 'italic'}}>اعداد را انگلیسی وارد کنید</Text>
          {this.renderVariants()}
        </ScrollView>
      </View>
  }
}

export class CategorySection extends React.Component {
  constructor(props){
    super(props)
    {
      this.state = {
        data :[],
        selectedData : [],
        categoryRefresher: null,
        existedCategoryNames : []
      }
    }
  }
  
  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  componentDidMount()
  {
    this.props.onRef(this)
    categories = {}

    if(this.props.goodinfo.hascategories === undefined || this.props.goodinfo.hascategories == null)
      return

    this.props.goodinfo.hascategories.forEach(x => {
      name = x.categorynameref
      route = x.categoryrouteref.split('-')

      if(route[0] == ' ')
        route[0] = name

      if(name in categories) {
        if(categories[name].length < route.length)
          categories[name] = route
      }
      else
      {
        categories[name] = route
      }
    }, categories)

    for(var name in categories)
    {
      this.state.existedCategoryNames.push(name)
      this.state.selectedData.push(categories[name].join(','))
    }

    console.log("added selected data: " + this.state.selectedData[0])
    this.setState({selectedData: this.state.selectedData})
  }

  setCategoryRefresher(data){
    this.setState({categoryRefresher: data})
  }
  
  updateData(data) {
    this.setState({data: data})
  }

  updateSelectedData(data)
  {
    this.setState({selectedData: data})
  }

  updateModel()
  {
    routes = []
    this.state.selectedData.forEach((selected) => {
      route = selected.split(',')
      name = route[0]
      route = route.join('-')
      goodid = this.props.goodinfo.id
      routes.push({goodid: goodid, categorynameref: name, categoryrouteref: route, details: "\{\}"})
    })


    routes.forEach(x =>{
      index = this.state.existedCategoryNames.indexOf(x.categorynameref)
      if(index >= 0)
        this.state.existedCategoryNames.splice(index,1)

      CategoryRepository.UpdateHasCategory(x).then(() => console.log('updated'))
    })

    this.state.existedCategoryNames.forEach(x =>{
      CategoryRepository.DeleteHasCategory(x, this.props.goodinfo.id).then(() => console.log('deleted'))
    })
  }


  render() 
  {    
    return <View style={{flexDirection: 'column', width: '100%', height: '100%', justifyContent: 'flex-start', backgroundColor: Colors.Back}}>

    <ScrollView>
      <CategoryHeirarchy 
        enableEditing = {false}
        data = {this.state.data} 
        selected = {this.state.selectedData} 
        setCategoryRefresher ={(params) => this.setCategoryRefresher(params)} 
        categoryRefresher = {this.state.categoryRefresher}
        updateCategoryData ={(params) => this.updateData(params)} 
        updateCategorySelectedData = {(params) => this.updateSelectedData(params)}/>
      </ScrollView> 
      </View>
  }
}


export class InfoSection extends React.PureComponent {

  render() 
  {
    return <View style={[{padding: 10}, { backgroundColor: Colors.Back }]}>
    <View style ={EditPageStyles.InfoSectionView}>

    <View style = {EditPageStyles.InfoSectionPartView}>
        <Text style = {EditPageStyles.InfoSectionLableText}>کد</Text>
        {/* <Selector data = {[
          ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'],
          [0,1,2,3,4,5,6,7,8,9],
          [0,1,2,3,4,5,6,7,8,9],
          [0,1,2,3,4,5,6,7,8,9],
          [0,1,2,3,4,5,6,7,8]
        ]}
          goodinfo = {this.props.goodinfo}
          onValueChange = {(params) => this.props.refresher(params)}
      /> */}
        <Input 
          value = {this.props.goodinfo.code}
          onChangeHandler={(value) => {
            this.props.goodinfo.code = value
            this.props.refresher(this.props.goodinfo)}}
        ></Input>
      </View>

    <View style = {EditPageStyles.InfoSectionSeperator}/>

    <View style = {EditPageStyles.InfoSectionPartView}>
      <Text style = {EditPageStyles.InfoSectionLableText}>موجودی</Text>
      <Switch trackColor = {Colors.WeakGold} thumbColor = {Colors.Gold} 
      value = {this.props.goodinfo.available} 
      onValueChange = {(value) => {
        this.props.goodinfo.available = value
        this.props.refresher(this.props.goodinfo)}}></Switch>
    </View>

      <View style = {EditPageStyles.InfoSectionSeperator}/>

<View style = {EditPageStyles.InfoSectionPartView}>
  <Text style = {EditPageStyles.InfoSectionLableText}>اجرت</Text>
  <Input 
    keyboardType = {'numeric'}
    value = {this.props.goodinfo.fee.toString()}
    onChangeHandler={(value) => {
      this.props.goodinfo.fee = value
      this.props.refresher(this.props.goodinfo)}}
  ></Input>
</View>


<View style = {EditPageStyles.InfoSectionSeperator}/>

<View style = {EditPageStyles.InfoSectionPartView}>
  <Text style = {EditPageStyles.InfoSectionLableText}>وزن</Text>
  <Input value = {this.props.goodinfo.weight.toString()}
  keyboardType = {'numeric'}
  onChangeHandler={(value) => {
    this.props.goodinfo.weight = value
    this.props.refresher(this.props.goodinfo)}}
  ></Input>
</View>


<View style = {EditPageStyles.InfoSectionSeperator}/>

<View style = {EditPageStyles.InfoSectionPartView}>
  <Text style = {EditPageStyles.InfoSectionLableText}>سایز</Text>
  <Input value = {this.props.goodinfo.size.toString()}
  keyboardType = {'numeric'}
  onChangeHandler={(value) => {
    this.props.goodinfo.size = value
    this.props.refresher(this.props.goodinfo)}}></Input>
</View>

      <View style = {EditPageStyles.InfoSectionSeperator}/>

      <View style = {EditPageStyles.InfoSectionPartView}>
        <Text style = {EditPageStyles.InfoSectionLableText}>کارگاه سازنده</Text>
        <Input value = {this.props.goodinfo.company}
        onChangeHandler={(value) => {
          this.props.goodinfo.company = value
          this.props.refresher(this.props.goodinfo)}}></Input>
      </View>
      
      <View style = {EditPageStyles.InfoSectionSeperator}/>

<View style = {EditPageStyles.InfoSectionPartView}>
  <Text style = {EditPageStyles.InfoSectionLableText}>رنگ بندی</Text>
  <Input value = {this.props.goodinfo.color}
  onChangeHandler={(value) => {
    this.props.goodinfo.color = value
    this.props.refresher(this.props.goodinfo)}}></Input>
</View>

      {/* <View style = {EditPageStyles.InfoSectionSeperator}/>

      <View style = {EditPageStyles.InfoSectionPartView}>
        <Text style = {EditPageStyles.InfoSectionLableText}>تاریخ</Text>
        <PersianDatePicker/>
      </View> */}


    </View>
  </View>
  }
}

export default class EditPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      submiting: false,
      goodinfo: props.navigation.getParam('goodinfo', selectedEntity),
      switch: false,
      index: 0,
      imagePaths : [],
      uploadProgresses: [],
      routes: [
        { key: 'first', title: 'اطلاعات' },
        { key: 'second', title: 'ضمیمه' },
        { key: 'third', title: 'گونه ها'},
        { key: 'forth', title: 'دسته بندی'}
      ],
  };

  this.refreshStat = this.refreshStat.bind(this);
}

refreshStat (newGoodInfo) {
  this.setState({goodinfo: newGoodInfo})
}

refreshImages (paths) {
  this.setState({imagePaths: paths})
}

addImage(url){
  var json = JSON.parse(this.state.goodinfo.pictures)
  json.push({uri: url})
  this.state.goodinfo.pictures = JSON.stringify(json)
  this.refreshStat(this.state.goodinfo)
}

imageDeleter (index) {
  this.state.imagePaths.splice(index, 1)
  this.state.uploadProgresses.splice(index, 1)
  this.setState({imagePaths: this.state.imagePaths, uploadProgresses: this.state.uploadProgresses});
}

serverImageDeleter(index) {
  var json = JSON.parse(this.state.goodinfo.pictures)
  json.splice(index, 1)
  this.state.goodinfo.pictures = JSON.stringify(json)
  this.refreshStat(this.state.goodinfo)
}

loadData() {
  GoodsRepository.getGood(this.state.goodinfo.id).then((data)=>{
    this.onLoadDataTweak(data)
    this.setState({goodinfo: data, loading: false})
  })
}

onLoadDataTweak (data){
  // null check
  data.available = (data.available == null)? (true):((data.available==1)?(true):(false));
  data.code = (data.code == null)? ('0'):(data.code);
  data.color = (data.color == null)? ('-'):(data.color);
  data.company = (data.company == null)? ('-'):(data.company);
  data.fee = (data.fee == null)? ('0'):(data.fee);
  data.size = (data.size == null)? ('0'):(data.size);
  data.weight = (data.weight == null)? ('0'):(data.weight);
  data.pictures = (data.pictures == null)? ('[]'):(data.pictures);
}

componentDidMount() {
  this.loadData()
}

  OpenImagePicker = () => {
    ImagePicker.openPicker({
      width: 1000,
      height: 600,
      cropping: true
    }).then(image => {
      this.state.imagePaths.push(image.path)
      this.setState({imagePaths: this.state.imagePaths});
      this.UploadImage(this.state.imagePaths.indexOf(image.path))
    });
  }
  //zn2w9azu

  UploadImage = (index) => {
    RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/dpit6bn8l/image/upload?upload_preset=zn2w9azu', {
      //... some headers,
      'Content-Type' : 'octet-stream',
    },
    [
      {
        name: 'folder',
        data: 'zargar'
      },
      {
        name : 'tags',
        data: 'goodies'
      },
      {
        name : 'file',
        filename : 'good' + this.state.goodinfo.id.toString() + index.toString(),
        data: RNFetchBlob.wrap(this.state.imagePaths[index])
      }])
    // listen to upload progress event, emit every 250ms
    .uploadProgress({ interval : 1 },(written, total) => {
        var progresses = this.state.uploadProgresses
        progresses[index] = (written / total) * 100
        this.setState({uploadProgresses : progresses})

    })
    // listen to download progress event, every 10%
    // .progress({ count : 10 }, (received, total) => {
    // })
    .then((resp) => {
      this.addImage( JSON.parse(resp.text()).url)
      this.imageDeleter(index)
      // ...
    })
    .catch((err) => {
      // ...
    })
  }

  submitChanges ()
  {
    this.setState({submiting: true})
    this.variantRef.updateModel();
    this.categoryRef.updateModel();

    this.state.goodinfo.modifieddate = new Date()
    GoodsRepository.updateGood(this.state.goodinfo).then((response) =>{
      this.setState({submiting: false})
    })
  }


  renderTabViewScene = ({ route }) => {
    switch (route.key) {
      case 'first':
        return <InfoSection 
          refresher = {(params) => this.refreshStat(params)} 
          goodinfo = {this.state.goodinfo}/>;

      case 'second':
        return  <AttachmentSection 
        uploader = {() => this.OpenImagePicker()} 
        refresher = {(params) => this.refreshStat(params)} 
        goodinfo = {this.state.goodinfo}
        imagePaths = {this.state.imagePaths}
        refreshImages = {(params) => this.refreshImages(params)} 
        imageDeleter ={(index) => this.imageDeleter(index)}
        serverImageDeleter ={(index) => this.serverImageDeleter(index)}
        uploadProgresses = {this.state.uploadProgresses}
        />;

      case 'third':
        return  <VariantSection 
          onRef={ref => (this.variantRef = ref)}
          refresher = {(params) => this.refreshStat(params)} 
          goodinfo = {this.state.goodinfo}/>;

      case 'forth':
        return  <CategorySection 
          onRef = {ref => (this.categoryRef = ref)}
          refresher = {(params) => this.refreshStat(params)}
          goodinfo = {this.state.goodinfo}/>;

      default:
        return null;
    }
  };

  renderTabViewPager = props => (
    Platform.OS === 'ios' ? <PagerExperimental {...props} GestureHandler={GestureHandler} /> : <PagerAndroid {...props}/>
  );
 
  render() {
    return (
      this.state.loading? (<ActivityIndicator/>) : (
      <View style ={{justifyContent: 'center', backgroundColor: Colors.Back, width: '100%', height: '100%'}}>
        <StatusBar
        hidden >
          </StatusBar>
        <View style ={{height: 100, flexDirection: 'row' ,justifyContent: 'center'}}>
          <CachedImage style={{ flex: 1}} url = {this.state.goodinfo.pictures != '[]'? JSON.parse(this.state.goodinfo.pictures)[0].uri : ''}/>
          <Text style={{position: 'absolute', fontSize: 26, fontWeight : 'bold', color: Colors.StrongFocus, alignContent: 'center', alignItems: 'center', alignSelf:'center'}}>{this.state.goodinfo.title}</Text>
        </View>
        <TabView
          renderTabBar={props =>
            <TabBar
              {...props}
              indicatorStyle ={{backgroundColor: Colors.Gold, width: tabViewInitialLayout.width / 4, height: '8%'}}
              pressColor= {Colors.Gold}
              labelStyle ={{color: Colors.StrongFocus}}
              style ={{backgroundColor: Colors.Back,width: Dimensions.get('window').width}}
            />}
          renderPager={this.renderTabViewPager}
          onIndexChange={index =>  this.setState({ index })}
          navigationState={this.state}
          renderScene = {this.renderTabViewScene}
          initialLayout={tabViewInitialLayout}
          useNativeDriver
        />
        <View style={{flexDirection: 'row'}}>
          {this.state.submiting ? <View style ={{flex: 1, backgroundColor: Colors.Submit, borderRadius: 0}}></View> :<Button  onPress ={() => this.submitChanges()} style ={{flex: 1, backgroundColor: Colors.Submit, borderRadius: 0}} textPadding = {5} title = "ذخیره"></Button>}
          <Button  onPress ={() => navigationReference.navigate('main')} style ={{flex: 1, backgroundColor: Colors.WeakAlarm, borderRadius: 0}} textPadding = {5} title = "بستن"></Button>
        </View>
      </View>)
    );
  }
}
