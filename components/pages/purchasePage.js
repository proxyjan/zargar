import React, {Component} from 'react';
import {Modal, Picker, View, Text, Image, RefreshControl, ScrollView, TouchableHighlight, Alert, TextInput, TouchableOpacity} from 'react-native';

import Button from '../base/button';
import Input from '../base/input';
import {GoodsRepository, PurchaseRepository, ProfilesRepository} from '../../repository';
import {CachedImage} from '../../network';
import {PDFGenerator, createPDF} from '../../pdfexporter';

//styles
import MainStyles, {PurchasePageStyles, Colors, TranslateToPersianNumbers, ToJalali} from '../../mainStyles';
import {Context, navigationReference} from '../../App';

export default class PurchasePage extends React.Component {
constructor(props){
  super(props)

  this.state ={
    description: '',
    refreshing: false
  }
}

calculateWeights(purchases){
  var totalWeight = 0;
  for(var i = 0; i < purchases.length; i++){
    var purchase = purchases[i]
    var weight = purchase.good.weight
    var count = purchase.count
    if(purchase.variantid != null)
    {
      if('weight' in purchase.properties)
      {
        var w = parseInt(purchase.properties['weight'])
        if(!isNaN(w))
          weight = w
      }
    }

    if(isNaN(weight) || isNaN(count))
      continue;
    else
      totalWeight += (weight * count)
  }

  return totalWeight
}

  static navigationOptions = {
    tabBarLabel: 'درخواست',
    tabBarIcon: ({tintColor}) => (
      <Image source = {require('../../resources/Icons/tab_bar_purchase.png')} style={[MainStyles.tabBarIcon, {tintColor: tintColor}]}/>
    )}

    renderCurrentPurchases(data){
      if(data.purchases.length > 0)
      {
        var limitExceeded = false
        var totalWeight = this.calculateWeights(data.purchases)
        if(totalWeight >= (data.profileData.weight == null ? 0 : data.profileData.weight))
          limitExceeded = true
        return <View style ={{padding: 5}}>
        <Text style ={{margin: 10, fontWeight: 'bold', fontSize: 16, alignSelf: 'center'}}>سبد خرید</Text>
        {data.purchases.map((x, index) => 
        <PurchaseEntity key ={index} purchase = {x} purchases = {data}></PurchaseEntity>
        )}
      
        <Text>محدودیت وزن : {TranslateToPersianNumbers(data.profileData.weight.toString())} گرم</Text>
        <Text>وزن کل : {TranslateToPersianNumbers(totalWeight.toString())} گرم</Text>
        <Input onChangeHandler = {(newText) =>{this.setState({description: newText})}} value ={this.state.description} numberOfLines ={3} isMultiLine ={true} placeHolder = {'توضیحات...'}></Input>
        <Button onPress ={() => {
          {limitExceeded? null : this.CreatePurchaseModel(data)}
        }} style ={{backgroundColor: (limitExceeded? Colors.Alarm : Colors.Submit), margin: 20, height: 50, width : '50%', alignSelf :'center'}} title ="تکمیل سفارش"></Button>
        </View>
      }
      else
      {
        return <Text style ={{margin: 30, fontWeight: 'bold', fontSize: 20, fontStyle: 'italic'}}>سبد خرید شما خالی است!</Text>
      }
    }

    CreatePurchaseModel(data)
    {
      // TODO
      // SERVER SHOULD NOT ALLOW CREATING OF PURCHASESGOODS THAT ARE FROM
      // DIFFERENT OWNERS AND HAVE SAME PURHCASE ID
      // THERE SHOULD BE DIFFERENT PURCHASES. RIGHT NOW WE JUST DO IT ON UI

      // DIFFERENT PURCHASES BASED ON Goods owners
      userpurchases = {} // user-purchase model
      data.purchases.forEach(p =>{
        if(p.goodowner != data.profileData.owner)
        {
          if(p.goodowner in userpurchases) 
            userpurchases[p.goodowner].push(p)
          else
            userpurchases[p.goodowner] = [p]
        }

      }, userpurchases, data)


      for(var pu in userpurchases){
        model = {createddate: new Date(), resolveddate: new Date(), description: this.state.description, state: 'بررسی نشده', owner: data.profileData.owner}
        PurchaseRepository.CreatePurchase(model).then(x => {

          goodmodels = []

          userpurchases[pu].forEach(p => {
            if(p.variantid == null)
              goodmodels.push({count: p.count, goodid: p.goodid, purchaseid: x.id, variantid: 0,variantend: '' })
            else
              goodmodels.push({count: p.count, goodid: p.goodid, purchaseid: x.id, variantid: p.variantid, variantend: p.variantend})
          },x,goodmodels)

          PurchaseRepository.postGoods(goodmodels).then(x => console.log(x))
        })
      }

      data.purchases = []
      data.updatePurchases(data.purchases)
    }


    render() {
      return (
        <Context.Consumer>
          {data => <View style ={{width: '100%', height: '100%', padding : 10, backgroundColor: Colors.Back}}>
          <ScrollView 
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={() => this.userpurchases === undefined ? null : this.userpurchases.handleRefresh()}
              colors ={[Colors.WeakFocus]}
              progressBackgroundColor = {Colors.WeakGold}/>}>
              {this.renderCurrentPurchases(data)}
              <View>
                <Text style ={{margin: 10, fontWeight:"bold", fontSize: 15}}>سفارشات</Text>
                <UserPurchases onRef={ref => (this.userpurchases = ref)} context = {data}></UserPurchases>
              </View>
          </ScrollView>
          </View>}
        </Context.Consumer>
      );
    }
  }

export class UserPurchases extends React.Component{
  constructor(props)
  {
    super(props)

    this.state = {
      purchases : [],
      purchaseInfo :[],
      requestInfo: [],
      profiles: []
    }

    this.handleRefresh = this.handleRefresh.bind(this)
  }

  setup()
  {
    this.state.purchaseInfo = []
    this.state.requestInfo = []
    this.state.profiles = []

    // TODO: THIS PURCHASE GET IS HUGE, if there going to be multiple users,
    // this need to be redo and get with some filtering and not all the purchases
    PurchaseRepository.getPurchases().then(x => {
      this.setState({purchases: x, purchasesLoaded: true}, () =>{

        this.state.purchases.forEach((purchase, index) => {
          if(purchase.owner != this.props.context.profileData.owner)
          {
            if(purchase.purchasegoods.length > 0)
            {
              if(purchase.purchasegoods[0].goodpurchaseFkrel.owner == this.props.context.profileData.owner)
              {
                // The requests are these
                this.state.requestInfo[index] = []
                purchase.purchasegoods.forEach(purchaseGoods =>{
                  GoodsRepository.getGood(purchaseGoods.goodid).then(g => {
                    ProfilesRepository.getProfile(purchase.owner).then((pd)=>{
                      this.state.profiles[index] = pd
                      this.setState({profiles: this.state.profiles})
                    })
                    this.state.requestInfo[index].push({pg: purchaseGoods, g: g})
                    this.setState({requestInfo: this.state.requestInfo})
                  }, purchase, index)
                }, purchase)
              }
            }
          }
          else
          {
            this.state.purchaseInfo[index] = []
            purchase.purchasegoods.forEach(purchaseGoods =>{
              GoodsRepository.getGood(purchaseGoods.goodid).then(g => {
                this.state.purchaseInfo[index].push({pg: purchaseGoods, g: g})
                this.setState({purchaseInfo: this.state.purchaseInfo})
              }, purchase, index)
            })
          }
        })
      })
    })
  }

  componentWillUnmount() {
    this.props.onRef(undefined)
  }

  componentDidMount()
  {
    this.props.onRef(this)
    this.setup()
  }

  getUrlOfItem= (pictures) =>
{
    json = JSON.parse(pictures) 
    if(json != null)
    {
        if(json.length > 0)
        {
            return json[0].uri
        }
    }

    return 'http://www.startscripting.com/wp-content/plugins/wp-ulike/assets/img/no-thumbnail.png'
}

  renderPurchases()
  { 
    var result = []
    this.state.purchaseInfo.forEach((info, index) => {
      purchase = this.state.purchases[index]
      result.push(<View key={'purchase' + purchase.id} style ={{marginTop: 10, flexDirection: 'row-reverse', backgroundColor: Colors.WeakBack, alignItems: 'center'}}>
        <Text style ={{margin: 5}}>سفارش در تاریخ</Text>
        <Text style ={{fontWeight: 'bold', fontSize: 13, margin: 5}}>{TranslateToPersianNumbers(ToJalali(purchase.createddate))}</Text>
        <Text style ={{fontWeight: 'bold', fontSize: 13, margin: 5}}>{purchase.state}</Text>
        <Text style ={{margin: 5}}>با کالاهای زیر:</Text>
      </View>)
      this.state.purchaseInfo[index].forEach((x, i) =>{
        result.push(<View key={'purchase' + index + 'good' + i} style ={{flexDirection: 'row-reverse', justifyContent: 'flex-start' , backgroundColor: Colors.WeakBack, alignItems: 'center'}}>
          <View style ={{height: 20, width: 20, borderRadius: 20, overflow: 'hidden'}}><CachedImage url = {this.getUrlOfItem(x.g.pictures)} resizeMode = 'cover' style={{alignSelf: 'center', width: 20, height: 20}} viewStyle={{height: 20, width:20}}></CachedImage></View>
          <Text style ={{margin: 4}}>{TranslateToPersianNumbers(x.pg.count.toString())} عدد</Text>
          <Text style ={{margin: 4}}>{x.g.title}</Text>
          {x.pg.variantend ? <Text style ={{margin: 4}}>{x.pg.variantend}</Text> : null}
        </View>)
      }, index, result)
      if(purchase.description != '')
      {
        result.push(<View key ={'description' + purchase.id} style ={{backgroundColor: Colors.WeakBack}}>
        <Text style ={{margin: 10, fontSize: 15, fontWeight: 'bold'}}>توضیحات</Text>
        <Text>{purchase.description}</Text>
         </View>)
      }
    }, result, this)

    return result
  }

  renderRequests()
  {
    var result = []
    this.state.requestInfo.forEach((info, index) => {
      purchase = this.state.purchases[index]
      result.push(<View key={'purchase' + purchase.id} style ={{marginTop: 10, flexDirection: 'row-reverse', backgroundColor: Colors.WeakGold, alignItems: 'center'}}>
        <TouchableOpacity onPress ={() => {
          console.log(this.state.profiles[index])
          navigationReference.navigate('user', {profile: this.state.profiles[index], currentProfile: this.props.context.profileData})
        }} style ={{margin: 5, height: 90, width: 90, borderRadius: 50, overflow:'hidden'}}>{this.state.profiles[index] === undefined ? null : <CachedImage url = {this.state.profiles[index].profilepictureurl} resizeMode = 'cover' style={{alignSelf: 'center', width: 90, height: 90}} viewStyle={{height: 90, width:90}}/>}</TouchableOpacity>
        <Text style ={{margin: 5}}>درخواست در تاریخ</Text>
        <Text style ={{fontWeight: 'bold', fontSize: 13, margin: 2}}>{TranslateToPersianNumbers(ToJalali(purchase.createddate))}</Text>
        <Picker
          style ={{flex: 1}}
          selectedValue={this.state.purchases[index].state}
          mode='dropdown'
          onValueChange={(itemValue, itemIndex) =>
            {
              model = this.state.purchases[index]
              model.resolveddate = new Date()
              model.state = itemValue
              PurchaseRepository.updatePurchase(model).then(() =>{
                this.setState({purchases: this.state.purchases})
              })
            }
          }>
          <Picker.Item label="بررسی نشده" value="بررسی نشده" />
          <Picker.Item label="بررسی شد" value="بررسی شد" />
          <Picker.Item label="آماده شد" value="آماده شد" />
        </Picker>
      </View>)
      this.state.requestInfo[index].forEach((x, i) =>{
        result.push(<View key={'purchase' + index + 'good' + i} style ={{flexDirection: 'row-reverse', justifyContent: 'flex-start' , backgroundColor: Colors.WeakGold, alignItems: 'center'}}>
          <View style ={{height: 30, width: 30,borderRadius: 30, overflow: 'hidden'}}><CachedImage url = {this.getUrlOfItem(x.g.pictures)} resizeMode = 'cover' style={{alignSelf: 'center', width: 30, height: 30}} viewStyle={{height: 30, width:30}}></CachedImage></View>
          <Text style ={{margin: 4}}>{TranslateToPersianNumbers(x.pg.count.toString())} عدد</Text>
          <Text style ={{margin: 4}}>{x.g.title}</Text>
          {x.pg.variantend ? <Text style ={{margin: 4}}>{x.pg.variantend}</Text> : null}
        </View>)
      }, index, result)
      if (purchase.description != '')
      { result.push(<View key ={'description' + purchase.id} style ={{backgroundColor: Colors.WeakGold}}>
         <Text style ={{margin: 10, fontSize: 15, fontWeight: 'bold'}}>توضیحات</Text>
        <Text>{purchase.description}</Text>
      </View>) }
        result.push(<Button style ={{flex:1, height: 50}} IconPath = {require('../../resources/Icons/print.png')} onPress ={() => {
        //PDFGenerator();
        createPDF(this.state.purchases[index]);
      }}></Button>)
    }, result, this)

    return result
  }

  handleRefresh()
  {
    this.setup()
  }


  render()
  {
    return <View>
      {this.renderRequests()}
      {this.renderPurchases()}
    </View>
  }
}

export class PurchaseEntity extends React.Component {
constructor(props){
  super(props)

  this.state = {
    goodinfo : {},
    url : 'http://www.startscripting.com/wp-content/plugins/wp-ulike/assets/img/no-thumbnail.png'
  }
}

componentDidMount(){
  GoodsRepository.getGood(this.props.purchase.goodid).then(x => {
    this.setState({goodinfo: x}, () =>{
      // calculate properties
      if(this.props.purchase.variantid != null)
      {
        weight = this.state.goodinfo.weight
        if(this.props.purchase.variantid != null)
        {
          if('weight' in this.props.purchase.properties)
            weight = this.props.purchase.properties['weight']
        }

        size = this.state.goodinfo.size
        if(this.props.purchase.variantid != null)
        {
          if('size' in this.props.purchase.properties)
            size = this.props.purchase.properties['size']
        }

        fee = this.state.goodinfo.fee
        if(this.props.purchase.variantid != null)
        {
          if('fee' in this.props.purchase.properties)
            fee = this.props.purchase.properties['fee']
        }

        this.setState({weight: weight, size: size, fee: fee})
      }
      else
      {
      
        this.setState({weight: this.state.goodinfo.weight, size: this.state.goodinfo.size, fee: this.state.goodinfo.fee})
      }

    })

    var pictures = this.state.goodinfo.pictures
    if(pictures === undefined)
    {
      return
    }
  
    json = JSON.parse(pictures) 
    if(json != null)
    {
        if(json.length > 0)
        {
          this.setState({url: json[0].uri})
        }
    }
  })
}

renderPurchaseCount ()
{
  return this.props.purchase.count
}

handlePurchase(isPlus){
  
  this.props.purchase.count = isPlus? this.props.purchase.count+1 : this.props.purchase.count - 1

  if(this.props.purchase.count == 0)
  {
    var index = this.props.purchases.purchases.indexOf(this.props.purchase)
    this.props.purchases.purchases.splice(index, 1)
  }

  this.props.purchases.updatePurchases(this.props.purchases.purchases)
}


renderDetails(){
  if('title' in this.state.goodinfo)
    {
      size = TranslateToPersianNumbers(this.state.size?  this.state.size.toString() : '0')
      weight = TranslateToPersianNumbers(this.state.weight? this.state.weight.toString() : '0')
      fee = TranslateToPersianNumbers(this.state.fee? this.state.fee.toString() : '0')

      return <View style ={{flexDirection: 'column', padding: 3, paddingRight: 7, paddingLeft: 7}}>
      <View style ={{padding: 5, flexDirection: 'row-reverse'}}>
        <Text style ={{fontSize: 17}}>{this.state.goodinfo.title}</Text>
        {this.props.purchase.variantid != null ? <Text style ={{marginLeft: 5, marginRight: 5}}>{this.props.purchase.variantend.join('-')}</Text> : null}
      </View>
        <Text>{size} سایز</Text>
        <Text>{weight} وزن</Text>
        <Text>{fee} اجرت</Text>
      </View>
    }
  else
    return null
}

  render() {
    return(
      <View style = {PurchasePageStyles.EntityView}>
        <View style = {PurchasePageStyles.EntityImageView}>
          <CachedImage resizeMode = 'cover' style={{alignSelf: 'center', width: '100%', height: '100%'}}   url = {this.state.url}></CachedImage>
        </View>
        <View style = {PurchasePageStyles.EntityDetailsView}>
          {this.renderDetails()}
        </View>
        <View style ={{flex: 3, padding: 10, flexDirection: 'row-reverse', justifyContent: 'center', alignItems: 'center'}}>
          <Button
          onPress ={() => this.handlePurchase(true)}
           title = "+" 
           style ={{width: 40, margin: 10}}></Button>
          <Text style ={{fontWeight:'bold'}}>{this.renderPurchaseCount()}</Text>
          <Button 
          onPress = {() => this.handlePurchase(false)}
          title = "-" 
          style ={{width: 40, margin: 10}}></Button>
        </View>
      </View>
    )
  }
}
