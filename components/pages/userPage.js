import React from 'react';
import {View, Text, AsyncStorage, ActivityIndicator, Image, Alert} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import Button from '../base/button';
import {CachedImage} from '../../network';

import {ProfilesRepository} from '../../repository';
import {navigationReference, apiLink} from '../../App';
import {token} from '../../repository';
import App, { Context } from '../../App';
import { Colors, UserPageStyle } from '../../mainStyles';
import { ScrollView } from 'react-native-gesture-handler';
import Input from '../base/input';

var RNFetchBlob = require('rn-fetch-blob').default;

export default class UserPage extends React.Component{
    constructor(props)
    {
      super(props)

      this.state = {
        profile : this.props.navigation.getParam("profile", null),
        currentProfile : this.props.navigation.getParam("currentProfile", null),
        isAdmin: (this.props.navigation.getParam("profile", {owner: -1}).owner == 3)?(true):(false),
        isSelf: (this.props.navigation.getParam("profile", {owner:-1}).owner == this.props.navigation.getParam("currentProfile", {owner: -1}).owner) ? (true) : (false),
        isAdminMyself: (this.props.navigation.getParam("currentProfile", {owner: -1}).owner == 3)?(true):(false),

        profilePicturePath: null,
        meliCardPath: null,
        parvanePath: null,

        profilePictureProgress: null,
        meliCardProfress: null,
        parvaneProgress: null,

        submiting: false,

      }


    }

    updateProfile(){
      // Update profile info, both on server and client if changes were applied and confirmed
    }

    refresh (newProfile) {
      this.setState({profile: newProfile})
    }

    imageDeleter (type) {
      if(type == "meli")
        this.setState({meliCardPath: "", meliCardProfress: 0})

      if(type == "parvane")
        this.setState({parvanePath: "", parvaneProgress: 0})

      if(type == "profile")
        this.setState({profilePicturePath: "", profilePictureProgress: 0})
    }

    serverImageDeleter(type) {
      if(type == "meli")
        this.state.profile.melicardscan = ""

      if(type == "parvane")
        this.state.profile.parvanescan = ""

      if(type == "profile")
        this.state.profile.profilepictureurl = ""

      this.refreshStat(this.state.goodinfo)
    }


    addImage(url, type){
      if(type == "meli")
        this.state.profile.melicardscan = url
    
      if(type == "parvane")
        this.state.profile.parvanescan = url

      if(type == "profile")
        this.state.profile.profilepictureurl = url

      this.refresh(this.state.profile)
    }

    OpenImagePicker = (type) => {

      var width = 500
      var height = 500
      if(type == "meli"){
        width = 1000
        heith = 700
      }

      if(type == "parvane"){
        width = 1400
        height = 2200
      }

      ImagePicker.openPicker({
        width: width,
        height: height,
        cropping: true
      }).then(image => {
        if(type == "meli"){
          this.state.meliCardPath = image.path
          this.setState({meliCardPath: this.state.meliCardPath});
        }
        
        if(type == "parvane"){
          this.state.parvanePath = image.path
          this.setState({parvanePath: this.state.parvanePath});
        }

        if(type == "profile"){
          this.state.profilePicturePath = image.path
          this.setState({profilePicturePath: this.state.profilePicturePath});
        }

        this.UploadImage(type)
      });
    }

    UploadImage = (type) => {
      var path = ""
      if(type == "profile"){
        path = this.state.profilePicturePath
      }

      if(type == "parvane"){
        path = this.state.parvanePath
      }

      if(type == "meli"){
        path = this.state.meliCardPath
      }

      RNFetchBlob.fetch('POST', 'https://api.cloudinary.com/v1_1/dpit6bn8l/image/upload?upload_preset=zn2w9azu', {
        'Content-Type' : 'octet-stream',
      },
      [
        {
          name: 'folder',
          data: 'zargar'
        },
        {
          name : 'tags',
          data: type
        },
        {
          name : 'file',
          filename : 'profile_owner' + this.state.profile.owner.toString() + type,
          data: RNFetchBlob.wrap(path)
        }])
      .uploadProgress({ interval : 1 },(written, total) => {

        if(type == "profile"){
          this.setState({profilePictureProgress : (written / total) * 100})
        }
        if(type == "meli"){
          this.setState({meliCardProfress : (written / total) * 100})
        }
        if(type == "parvane"){
          this.setState({parvaneProgress : (written / total) * 100})
        }
  
      })
      .then((resp) => {
        this.addImage( JSON.parse(resp.text()).url, type)
        this.imageDeleter(type)
        // ...
      })
      .catch((err) => {
        // ...
      })
    }

    record(label, property){
      if(this.state.profile[property] == null){
        this.state.profile[property] = "";
      }
      return <View style ={{flexDirection: 'row-reverse', alignContent: 'center', alignItems: 'center'}}>
        <Text>{label}</Text>
        <Text style={{fontSize: 16, padding: 5}} >{this.state.profile[property].toString()}</Text>
      </View>
    }

    inputRecord(label, property, keyboardType = null){
      if(this.state.profile[property] == null){
        this.state.profile[property] = "";
      }
      return <View style ={{flexDirection: 'row-reverse', alignContent: 'center', alignItems: 'center'}}>
        <Text>{label}</Text>
        <Input value = {this.state.profile[property].toString()}
              keyboardType ={keyboardType}
              onChangeHandler={(value) => {
                this.state.profile[property] = value
                this.refresh(this.state.profile)}}
              ></Input>
      </View>
    }

    image(type){
      var url = ""
      var title = ""
      var height = 300

      if(type == "profile"){
        url = this.state.profile["profilepictureurl"]
        title = "عکس پروفایل"
        height = 350
      }

      if(type == "parvane") {
        url = this.state.profile["parvanescan"]
        title = "پروانه ی کسب"
        height = 700
      }

      if(type == "meli") {
        url = this.state.profile["melicardscan"]
        title = "کارت ملی"
        height = 250
      }
      
      return <View style ={{backgroundColor: Colors.WeakBack, paddingBottom: 10}}>
          <CachedImage resizeMode = 'center' style ={{flex: 1,margin: 5, width: '100%', height: height, alignSelf: 'center'}} url ={url}/>
          <Text style ={{padding: 5, height: 40, fontSize: 20}}>{title}</Text>
        </View>
    }

    inputImage(type){
      var url = ""
      var title = ""
      var height = 300

      if(type == "profile"){
        url = this.state.profile["profilepictureurl"]
        title = "عکس پروفایل"
        height = 350
      }

      if(type == "parvane") {
        url = this.state.profile["parvanescan"]
        title = "پروانه ی کسب"
        height = 700
      }

      if(type == "meli") {
        url = this.state.profile["melicardscan"]
        title = "کارت ملی"
        height = 250
      }

      
      return <View style ={{backgroundColor: Colors.WeakBack, paddingBottom: 10}}>
          <CachedImage resizeMode = 'center' style ={{flex: 1,margin: 5, width: '100%', height: height, alignSelf: 'center'}} url ={url}/>
          <Button style ={{height: 40}} onPress = {() => this.OpenImagePicker(type)}  title ={title}></Button>
        </View>
    }

    submitChanges(){
      this.setState({submiting: true})
      if(this.state.isAdminMyself){
        if(!isNaN(this.state.profile["weight"])){
          ProfilesRepository.updateProfile(this.state.profile).then((x) => {
            this.setState({submiting: false})
            this.refresh(this.state.profile)
          })
        }
        else{
          Alert.alert(
            'خطا در ورودی',
            'مقدار وزن صحیح نمیباشد!',
            [
              {text: 'اوکی', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false},
          );
          this.setState({submiting: false})
        }
      }
      else
      {
        ProfilesRepository.updateProfileUser(this.state.profile.owner, this.state.profile).then((x) => {
          this.setState({submiting: false})
          this.refresh(this.state.profile)
        })
      }
    }

    renderForAdminSelf(){
      return <View>
          {this.inputImage("profile")}
          {this.inputRecord("اسم کوچک", "firstname")}
          {this.inputRecord("اسم میانی", "middlename")}
          {this.inputRecord("فاملی", "lastname")}
          {this.inputRecord("شهرستان", "city")}
          {this.inputRecord("آدرس", "address")}
          {this.inputRecord("شرکت", "companynumber")}
          {this.inputRecord("شماره همراه", "mobilenumber")}
          {this.inputRecord("شماره تلفن", "phonenumber")}
      </View>
    }

    renderForUserSelf(){
      return <View>
          {this.inputImage("profile")}
          {this.record("محدودیت وزن", "weight")}
          {this.inputRecord("اسم کوچک", "firstname")}
          {this.inputRecord("اسم میانی", "middlename")}
          {this.inputRecord("فاملی", "lastname")}
          {this.inputRecord("شهرستان", "city")}
          {this.inputRecord("آدرس", "address")}
          {this.inputRecord("شرکت", "companynumber")}
          {this.inputRecord("شماره همراه", "mobilenumber")}
          {this.inputRecord("شماره تلفن", "phonenumber")}
          {this.inputImage("meli")}
          {this.inputImage("parvane")}
      </View>
    }

    renderUserForAdmin(){
      return <View>
          {this.image("profile")}
          {this.inputRecord("محدودیت وزن", "weight", 'numeric')}
          {this.record("اسم کوچک", "firstname")}
          {this.record("اسم میانی", "middlename")}
          {this.record("فاملی", "lastname")}
          {this.record("شهرستان", "city")}
          {this.record("آدرس", "address")}
          {this.record("شرکت", "companynumber")}
          {this.record("شماره همراه", "mobilenumber")}
          {this.record("شماره تلفن", "phonenumber")}
          {this.image("meli")}
          {this.image("parvane")}
      </View>
    }

    renderUserForUser(){
      return <View>
          {this.image("profile")}
          {this.record("اسم کوچک", "firstname")}
          {this.record("اسم میانی", "middlename")}
          {this.record("فاملی", "lastname")}
      </View>
    }

    renderBaseOnPermission(){
      if(this.state.isAdmin && this.state.isAdminMyself){
        return this.renderForAdminSelf()
      }
      else if(!this.state.isAdminMyself && this.state.isSelf){
        return this.renderForUserSelf()
      }
      else if(this.state.isAdminMyself && !this.state.isSelf){
        return this.renderUserForAdmin()
      }
      else if(!this.state.isAdminMyself && !this.state.isSelf){
        return this.renderUserForUser()
      }
    }

    render()
    {
        return <ScrollView style = {UserPageStyle.MainView}>
          <View style ={UserPageStyle.TopView}>
              <Button onPress ={() => {this.submitChanges()}} style ={{backgroundColor: Colors.Submit}} title={this.state.submiting == true? "o" : "تایید تغییرات"}></Button>
              {this.renderBaseOnPermission()}
          </View>

        </ScrollView>
    }
}
