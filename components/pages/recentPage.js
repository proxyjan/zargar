import React from 'react';
import {View, ActivityIndicator, FlatList, Image, Text, RefreshControl} from 'react-native';

// Components
import RecordEntity from '../recordEntity';
import TextEntity from '../textEntity';
import getImageAttachment from '../../HelperFunctions';
import {Menu, MenuOptions, MenuTrigger, renderers, withMenuContext} from 'react-native-popup-menu';
import {GoodsRepository} from '../../repository';

import MainStyles, {Colors} from '../../mainStyles';
import {apiLink} from '../../App';

export default class RecentScreen extends React.Component {
  static navigationOptions = {
      tabBarLabel: 'خانه',
      tabBarIcon: ({tintColor}) => (
        <Image source = {require('../../resources/Icons/tab_bar_home.png')} style={[MainStyles.tabBarIcon,{tintColor: tintColor}]}/>
      )}

    render() 
    {
      return (
        <View>
          <RecentFlatScroll/>
        </View>
      );
    }
}

export class RecentFlatScroll extends React.PureComponent {
    constructor(props) {
      super(props);
  
      this.state = {
        loading: true,
        data: [],
        page: 1,
        url : "Unknown",
        seed: 1,
        ImageView : null,
        error: null,
        refreshing: false,
      };
    }

    componentDidMount() {
      this.makeRemoteRequest();
    
    }

    makeRemoteRequest = () => {
      GoodsRepository.getGoods(0,100)
      .then((d) => {this.setData(d)})
      .catch((e) => {() => alert("خطا در گرفتن اطلاعات!" + e)});
    };

    setData = (d) => {
      this.setState({data: d, refreshing: false, loading: false})
    }
  
    renderSeparator = () => {
      return (
        <View
          style={
            {
              height: 20,
              backgroundColor: Colors.Back
            }
          }
        />
      );
    };
  
    renderHeader = () => {
      return (null)
    };
  
    renderFooter = () => {
      if(!this.state.loading) return null

      return <View>
        <ActivityIndicator color = {Colors.Gold} animating></ActivityIndicator>
      </View>
    };

    handleRefresh = () => {
      this.setState(
        {
          page: 1,
          seed: this.state.seed + 1,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );

      return true;
    };
  
    handleLoadMore = () => {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          this.makeRemoteRequest();
        }
      );

      return true;
    };

    getFromItem = (item, key) => {

      if(item.pictures === "null" || item.pictures === null || item.pictures === "" || item.pictures === "{}" || typeof item.pictures === "undefined")
      {
        return null;
      }
      else
      {
        return JSON.parse(item.pictures)[key];
      }
    }

    deleteRecordHanlder = () => {
      this.makeRemoteRequest()
    }

    renderItem = (item) => {
          return(
          <View>
              <RecordEntity
                onDelete = {this.deleteRecordHanlder}
                entity = {item}
                showOptions = {true}
                title = {item.name}
                url = {this.getFromItem(item, "url")}
                thumbnail = {this.getFromItem(item, "thumbnail")}
            />
          </View>
        )
    }

    render() 
    {
      return (
          <FlatList
            data={this.state.data}
            renderItem={({ item }) => (this.renderItem(item))}
            keyExtractor={item => item.id.toString()}
            ItemSeparatorComponent = {this.renderSeparator}
            ListHeaderComponent = {this.renderHeader}
            ListFooterComponent = {this.renderFooter}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            refreshControl={
              <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this.handleRefresh}
                  colors ={[Colors.WeakFocus]}
                  progressBackgroundColor = {Colors.WeakGold}
               />
            }
          />
      );
    }
  }