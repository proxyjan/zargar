import React from 'react';
import {View, Text, AsyncStorage, ActivityIndicator, Dimensions} from 'react-native';
import Input from '../base/input';
import Button from '../base/button';

import {ProfilesRepository, GoodsRepository, PurchaseRepository, LikeRepository} from '../../repository';
import {navigationReference, apiLink} from '../../App';
import {token} from '../../repository';
import App, { Context } from '../../App';
import { Colors } from '../../mainStyles';
import { ScrollView } from 'react-native-gesture-handler';
import {Menu, MenuOptions, MenuTrigger, renderers, withMenuContext} from 'react-native-popup-menu';
import {CachedImage} from '../../network';
import RecordEntity from '../recordEntity';

const menuHeight = Dimensions.get('window').height * .7

var RNFetchBlob = require('rn-fetch-blob').default;


const Openner = (props) => (
    <Menu 
        renderer={renderers.SlideInMenu}>
        <MenuTrigger>
            <View style ={{width: '100%', height: '100%', borderRadius: 3,  overflow: 'hidden'}}>
                <CachedImage resizeMode = 'cover' 
                    style={{alignSelf: 'center', width: '100%', height: '100%'}}  
                    url ={getUrlOfItem(props.entity.pictures)}/>
            </View>
        </MenuTrigger>
        <MenuOptions style ={{borderRadius: 10}}>
            <ScrollView style ={{maxHeight: menuHeight, borderRadius: 5}}>
                <RecordEntity entity = {props.entity} showOptions = {false}/>
            </ScrollView>
        </MenuOptions>
    </Menu> 
  );

const ContextOpenner = withMenuContext(Openner);

export default class AdminPage extends React.Component{
    constructor(props){
        super(props)

        this.state= {
            loading: true,
            highest: null,

            goodMostLike: null,
            goodMostLikeCount: null,
            goodMostDislike: null,
            goodMostDislikeCount: null,
            goodMostSell: null,
            goodMostSellCount: null,
            goodLeastSell: null,
            goodLeastSellCount: null
        }

        this.setup()
    }

    setup() {
        this.analysis()
    }

    goodRecord(title, good, info){
        if(good == null)
            return null
        return <View style ={{flexDirection :'row-reverse', width: '100%', alignContent: 'space-between', padding: 5, borderBottomWidth: 2, borderColor: Colors.Gold, alignItems: 'center', alignSelf:'stretch'}}>
            <View style ={{ flexDirection: 'column'}}>
                <Text style ={{fontSize: 25}}>{title}</Text>
                <Text style ={{fontSize: 14}}>{info}</Text>
            </View>
            <View style ={{flex : 1}}></View>
            <Text style ={{padding: 4, fontSize: 20}}>{good.title}</Text>
            <View style ={{height: 100, width: 100}}><ContextOpenner entity ={good}/></View>
        </View>
    }

    async analysis(){
        // Fetch information
        console.log("anaylize started")
        GoodsRepository.getGoods().then(goods => {
            PurchaseRepository.getPurchases().then(purchases => {
                PurchaseRepository.getPurchaseGoods().then(purchaseGoods => {
                    LikeRepository.GetAllLikes().then(likes => {
                        LikeRepository.GetAllDisLikes().then(dislikes => {

                            var goodslikes = {}
                            var goodsdislikes = {}
                            var pgs = {}

                            for (var i = 0; i < likes.length; i++){
                                var like = likes[i]
                                if(goodslikes[like.goodid] == null || goodslikes[like.goodid] == undefined)
                                    goodslikes[like.goodid] = 1
                                else
                                    goodslikes[like.goodid] += 1
                            }

                            for (var i = 0; i < dislikes.length; i++){
                                var dislike = dislikes[i]
                                if(goodsdislikes[dislike.goodid] == null || goodsdislikes[dislike.goodid] == undefined)
                                    goodsdislikes[dislike.goodid] = 1
                                else
                                    goodsdislikes[dislike.goodid] += 1
                            }

                            for (var i = 0; i < purchaseGoods.length; i++){
                                var pg = purchaseGoods[i]
                                if(pgs[pg.goodid] == null || pgs[pg.goodid] == undefined)
                                    pgs[pg.goodid] = pg.count
                                else
                                    pgs[pg.goodid] += pg.count
                            }


                            maxLike = -1
                            maxLikeGoodId = 0
                            for (var prop in goodslikes) {
                                if (Object.prototype.hasOwnProperty.call(goodslikes, prop)) {
                                    if(goodslikes[prop] > maxLike)
                                    {
                                        maxLikeGoodId = prop
                                        maxLike = goodslikes[prop]
                                    }
                                }
                            }

                            maxDislike = -1
                            maxDislikeGoodId = 0
                            for (var prop in goodsdislikes) {
                                if (Object.prototype.hasOwnProperty.call(goodsdislikes, prop)) {
                                    if(goodsdislikes[prop] > maxDislike)
                                    {
                                        maxDislikeGoodId = prop
                                        maxDislike = goodsdislikes[prop]
                                    }
                                }
                            }

                            mostSell = -1
                            leastSell = 1000000000000
                            mostSellGoodId = 0
                            leastSellGoodId = 0
                            for (var prop in pgs) {
                                if (Object.prototype.hasOwnProperty.call(pgs, prop)) {
                                    if(pgs[prop] > mostSell)
                                    {
                                        mostSellGoodId = prop
                                        mostSell = pgs[prop]
                                    }
                                    if(pgs[prop] < leastSell)
                                    {
                                        leastSellGoodId = prop
                                        leastSell = pgs[prop]
                                    }
                                }
                            }

                            GoodsRepository.getGood(maxLikeGoodId)
                            .then(maxLikeGood => {
                                this.setState({
                                    goodMostLike: maxLikeGood,
                                    goodMostLikeCount: maxLike,
                                    loading: false})
                            })

                            GoodsRepository.getGood(maxDislikeGoodId)
                            .then(maxDislikeGood => {
                                this.setState({
                                    goodMostDislike: maxDislikeGood,
                                    goodMostDislikeCount: maxDislike,
                                    loading: false})
                            })

                            GoodsRepository.getGood(mostSellGoodId).then(mostSellGood => {
                                this.setState({
                                    goodMostSell: mostSellGood,
                                    goodMostSellCount: mostSell,
                                    loading: false})
                            })

                            GoodsRepository.getGood(leastSellGoodId)
                            .then(leastSellGood => {
                                this.setState({
                                    goodLeastSell: leastSellGood,
                                    goodLeastSellCount: leastSell,
                                    loading: false})
                            })
                        })
                    })
                })

            })
        })

    }

    render()
    {
        if(this.state.loading){
            return <View><ActivityIndicator color = {Colors.Gold} animating></ActivityIndicator></View>
        }
        else return <ScrollView style ={{flexDirection: 'column'}}>
            {this.goodRecord(`بیشترین بازخورد`, this.state.goodMostLike, `تعداد: ${this.state.goodMostLikeCount}`)}
            {this.goodRecord(`بیشترین انتقاد`, this.state.goodMostDislike, `تعداد: ${this.state.goodMostDislikeCount}`)}
            {this.goodRecord(`بیشترین فروش`, this.state.goodMostSell, `${this.state.goodMostSellCount} عدد`)}
            {this.goodRecord(`کم ترین فروش`, this.state.goodLeastSell, `${this.state.goodLeastSellCount} عدد`)}
        </ScrollView>

    }
}
