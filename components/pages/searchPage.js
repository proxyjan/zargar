import React from 'react';
import {View, Text, Image, RefreshControl, ScrollView, Dimensions} from 'react-native';
import {FlatGrid}  from 'react-native-super-grid';
import {Menu, MenuOptions, MenuTrigger, renderers, withMenuContext} from 'react-native-popup-menu';

import _ from 'lodash';
// Styles
import MainStyles, {Colors} from '../../mainStyles';

import {contains} from '../../HelperFunctions';
import {Context} from '../../App';

import CategoryHeirarchy from '../categoryHeirarchy';
import FilterSection from '../sections/filterSection';
import RecordEntity from '../recordEntity';
import CreateRecord from '../sections/createRecord';

import Input from '../base/input';
import Button from '../base/button';
import ActionButton from '../base/actionButton';
import {GoodsRepository } from '../../repository';
import {CachedImage} from '../../network';

const menuHeight = Dimensions.get('window').height * .7

getUrlOfItem= (pictures) =>
{
    json = JSON.parse(pictures) 
    if(json != null)
    {
        if(json.length > 0)
        {
            return json[0].uri
        }
    }

    return 'http://www.startscripting.com/wp-content/plugins/wp-ulike/assets/img/no-thumbnail.png'
}

const Openner = (props) => (
    <Menu 
        renderer={renderers.SlideInMenu}>
        <MenuTrigger>
            <View style ={{width: '100%', height: '100%', borderRadius: 3,  overflow: 'hidden'}}>
                <CachedImage resizeMode = 'cover' 
                    style={{alignSelf: 'center', width: '100%', height: '100%'}}  
                    url ={getUrlOfItem(props.entity.pictures)}/>
            </View>
        </MenuTrigger>
        <MenuOptions style ={{borderRadius: 10}}>
            <ScrollView style ={{maxHeight: menuHeight, borderRadius: 5}}>
                <RecordEntity entity = {props.entity} showOptions = {false}/>
            </ScrollView>
        </MenuOptions>
    </Menu> 
  );

const CreateOpenner = (props) => (
    <ActionButton 
        title ="+" 
        right = {'3%'} 
        bottom = {'3%'} 
        width = {60}
        height = {60}
        onPress ={() => props.ctx.menuActions.openMenu('create')}/>
);

const CreateSection = (props) => (
    <CreateRecord  
        categoryRefresher = {props.categoryRefresher}
        selected = {props.selected}  
        ctx = {props.ctx}></CreateRecord>
);

const ContextOpenner = withMenuContext(Openner);
const ContextCreateOpenner = withMenuContext(CreateOpenner);
const ContextCreateSection = withMenuContext(CreateSection)

export default class SearchScreen extends React.Component {
    static navigationOptions = {
        tabBarLabel: 'محصولات',
        tabBarIcon: ({tintColor}) => (
          <Image source = {require('../../resources/Icons/ic_action_stags.png')} style={[MainStyles.tabBarIcon, {tintColor: tintColor}]}/>
        )}

    render() {
        return (
            <CategoryList parentUpdater = {this.totalUpdater}/>
        );
    }
}

export class SearchSection extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            borderColor: Colors.WeakFocus
        }
    }

    onFocus =() => {
        this.setState({
            borderColor: Colors.Gold
        })
    }
    
    onBlur = () => {
        this.setState({
            
            borderColor: Colors.WeakFocus
        })
    }

    render()
    {
        return(
            <View style={MainStyles.SearchPageSearchSection}> 
                <Input style ={{flex: 1}} isMultiLine ={false} placeHolder = {'جستجو'} onChangeHandler = {this.props.onChangeHandler}/>
            </View>
        )
    };
}

export class CategoryList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            fullData:[],
            data:[],
            query: "",
            error: null,
            refreshing: false,
            categoryData: [],
            categorySelectedData : [],
            categoryRefresher: null,
            maxF: 100,
            minF: 0,
            maxW: 100,
            minW: 0,
            maxS: 10,
            minS:0,
            rangeValuesF : [0,100],
            rangeValuesW : [0,100],
            rangeValuesS : [0,100],
            isRangeSetup: false,
        };

        this.rangeHanlderS = this.rangeHanlderS.bind(this)
        this.rangeHanlderF = this.rangeHanlderF.bind(this)
        this.rangeHanlderW = this.rangeHanlderW.bind(this)
        this.handleRefresh = this.handleRefresh.bind(this)
    }

    componentDidMount() {
        this.makeRemoteRequest()
    };

    setCategoryRefresher(d){
        this.setState({categoryRefresher: d})
    }

    updateCategoryData(data) {
        this.setState({categoryData: data})
    }

    updateCategorySelectedData(data)
    {
        this.setState({categorySelectedData: data}, () => this.filter())
    }

    renderCreateMenu = (ownerId) => {
        if(ownerId != 3)
            return null;
        else
            return <Menu 
                name='create' 
                ref={this.onRef} renderer={renderers.SlideInMenu}>
                <MenuTrigger/>
                <MenuOptions>
                    <ContextCreateSection 
                        categoryRefresher = {this.state.categoryRefresher}
                        selected = {this.state.categorySelectedData}
                    ></ContextCreateSection>
                </MenuOptions>
            </Menu> 
    }

    renderPageHeader = () => {
        return (
            <View style={MainStyles.Shadow}>
                <CategoryHeirarchy 
                    data = {this.state.categoryData} 
                    selected = {this.state.categorySelectedData} 
                    setCategoryRefresher ={(params) => this.setCategoryRefresher(params)} 
                    categoryRefresher = {this.state.categoryRefresher}
                    updateCategoryData ={(params) => this.updateCategoryData(params)} 
                    updateCategorySelectedData = {(params) => this.updateCategorySelectedData(params)}/>
                <FilterSection 
                rangeHanlderS = {this.rangeHanlderS} 
                rangeHanlderF = {this.rangeHanlderF} 
                rangeHanlderW = {this.rangeHanlderW} 
                valuesW = {this.state.rangeValuesW} 
                valuesS ={this.state.rangeValuesS} 
                valuesF ={this.state.rangeValuesF} 
                maxS = {this.state.maxS}
                minS = {this.state.minS}
                maxW = {this.state.maxW}
                minW = {this.state.minW}
                maxF ={this.state.maxF} 
                minF = {this.state.minF}/>
                <SearchSection onChangeHandler = {this.handleSearch}/>
            </View>
        )
    };

    handleSearch = (newText) =>
    {
        this.filter(newText)
    }

    makeRemoteRequest = () => {
        if (typeof this.state.categoryRefresher === "function"){
            this.state.categoryRefresher()
        }
        GoodsRepository.getGoods(1,1).then((d) => {this.setData(d)});
      };
  
    setData = (d) => {
        this.setState({fullData: d, refreshing: false, loading: false}, ()=>{
            this.filter()
        })
    }

    rangeHanlderW(values)
    {
        this.setState({rangeValuesW: values}, () => this.filter())
    }

    rangeHanlderF(values)
    {
        this.setState({rangeValuesF: values}, () => this.filter())
    }

    rangeHanlderS(values)
    {
        this.setState({rangeValuesS: values}, () => this.filter())
    }

    filter(queryText)
    {
        this.state.data = []
        maxF = 0
        minF = 0
        maxW = 0
        minW = 0
        maxS = 0
        minS = 0

        newData = this.state.fullData.filter(data => {
            categoryCondition = true
            searchCondition = true
            rangeCondition = true 


            // category condition
            this.state.categorySelectedData.forEach((selected)=>{
                cc = false
                data.hascategories.forEach(category => {
                    newS = selected.split(',').join('-')
                    cc = cc || newS == category.categoryrouteref
                }, selected, data, cc)
                categoryCondition = categoryCondition && cc
            }, data, categoryCondition)

            // search condition
            if(queryText != null)
            {
                if(queryText != '')
                {
                    // search title condition
                    titlecondition = data.title != null ? data.title.includes(queryText) : false
                    // search code condition
                    codecondition = data.code != null ? data.code.includes(queryText) : false

                    searchCondition = titlecondition || codecondition
                }
            }

            // Range setup FEE
            if(data.fee != null)
            {
                if(this.state.isRangeSetup)
                    rangeCondition = (data.fee >= this.state.rangeValuesF[0] 
                        && data.fee <= this.state.rangeValuesF[1])
                else
                {
                    if(data.fee > maxF)
                        maxF = data.fee
                    
                    if(data.fee < minF)
                        minF = data.fee   
                }   
            }

            // Range setup Weight
            if(data.weight != null)
            {
                if(this.state.isRangeSetup)
                    rangeCondition = rangeCondition && (data.weight >= this.state.rangeValuesW[0] 
                        && data.weight <= this.state.rangeValuesW[1])
                else
                {
                    if(data.weight > maxW)
                        maxW = data.weight
                    
                    if(data.weight < minW)
                        minW = data.weight   
                }   
            }

            // Range setup SIZE
            if(data.size != null)
            {
                if(this.state.isRangeSetup)
                    rangeCondition = rangeCondition && (data.size >= this.state.rangeValuesS[0] 
                        && data.size <= this.state.rangeValuesS[1])
                else
                {
                    if(data.size > maxS)
                        maxS = data.size
                    
                    if(data.size < minS)
                        minS = data.size   
                }   
            }

            return (categoryCondition && searchCondition && rangeCondition)
        }, minF, maxF, minS, maxS, minW, maxW, this)


        if(this.state.isRangeSetup)
            this.setState({data: newData})
        else
            this.setState({data: newData, 
                minW: minW,
                maxW: maxW,
                minS: minS,
                maxS: maxS,
                minF: minF, 
                maxF: maxF, 
                isRangeSetup: true, 
                rangeValuesF: [minF, maxF],
                rangeValuesS: [minS, maxS],
                rangeValuesW: [minW, maxW]
            })
    }

    renderItem = (item, index) => {
        return(
        <View style={[MainStyles.SearchPageItemContainer]}>
            <ContextOpenner entity = {item}/>
            <View style ={{width: '100%', alignItems : 'center', position: 'absolute', bottom: 0, backgroundColor: Colors.VeryWeakFocus, opacity: .6}}>
                <Text style ={ MainStyles.SearchPageItemText} key={index}>{'(' + item.code + ') ' + item.title}</Text>
            </View>
        </View>
        )
    }

    handleRefresh(){
        this.makeRemoteRequest()
        return true
    }

    render()
    {
        return(
            <Context.Consumer>
            { data => 
            <View style={{flexDirection: 'column', width: '100%', height: '100%', justifyContent: 'flex-start', backgroundColor: Colors.Back}}>
                <ScrollView 
                 refreshControl={
                    <RefreshControl
                      refreshing={this.state.refreshing}
                      onRefresh={this.handleRefresh}
                      colors ={[Colors.WeakFocus]}
                      progressBackgroundColor = {Colors.WeakGold}
                    />
                  }
                style ={{ flex:1 , width: '100%'}}>
                    <FlatGrid
                    itemDimension={100}
                    items ={this.state.data}
                    style={MainStyles.SearchPageGridView}
                    renderItem={({item, index}) => (this.renderItem(item, index))}
                    keyExtractor={(items, index) =>  (items[index] === undefined) ? ("0") : items[index].id.toString()}
                    ListHeaderComponent ={this.renderPageHeader}
                    ListFooterComponent ={<View style ={{height: 90}}></View>}
                    spacing = {4}
                    />
                </ScrollView>
                {this.renderCreateMenu(data.profileData.owner)}
                {data.profileData.owner == 3? <ContextCreateOpenner/> : null}
            </View>}
            </Context.Consumer>
        );
    }
}
