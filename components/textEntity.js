import React from 'react';
import {View, Text} from 'react-native';

// Styles
import MainStyles from '../mainStyles';
  
export default class TextEntity extends React.Component {
  render(){
    return (
      <View style = { MainStyles.RecentPageRecordEntityView}>
        <Text style={MainStyles.PostsText}>{this.props.title}</Text>
      </View>
    );
  }
}