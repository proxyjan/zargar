import PDFLib, { PDFDocument, PDFPage } from 'react-native-pdf-lib';
import {PermissionsAndroid} from 'react-native';
import RNHTMLtoPDF from 'react-native-html-to-pdf';
import {GoodsRepository, ProfilesRepository, VariantRepository, PurchaseRepository} from './repository';
import  {ToJalali, TranslateToPersianNumbers} from './mainStyles';

var RNFetchBlob = require('rn-fetch-blob').default;

const android = RNFetchBlob.android;
const downloadDir = RNFetchBlob.fs.dirs.DownloadDir;

async function requestWritePermission() {
    return PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Write Permission',
        message: 'For writing in Download location',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
}

async function purchaseToPrint(purchase){
  return new Promise((resolve, reject) => {
    var printInfo = new Object()
    printInfo.date = purchase.createddate
    printInfo.description = purchase.description
    printInfo.state = purchase.state

    ProfilesRepository.getProfile(purchase.owner).then(async (pd)=>{
      printInfo.name = pd.firstname + " " + pd.middlename + " " + pd.lastname
      printInfo.number = pd.phonenumber + " / " + pd.mobilenumber
      printInfo.city = pd.city

      printInfo["goods"] = []
      for (var i = 0; i < purchase.purchasegoods.length; i++){
        var purchaseGood = purchase.purchasegoods[i]
          printInfo.goods[i] = new Object()
          printInfo.goods[i].goodName = purchaseGood.goodpurchaseFkrel.title
          printInfo.goods[i].count = purchaseGood.count
          printInfo.goods[i].weight = purchaseGood.goodpurchaseFkrel.weight
          printInfo.goods[i].size = purchaseGood.goodpurchaseFkrel.size
    
          // check variant
          if(purchaseGood.variant != undefined && purchaseGood.variant != null){
            var fields = purchaseGood.variant.fields.split('-')
            var routes = JSON.parse(purchaseGood.variant.routes)
            var end = purchaseGood.variantend.split(',')
            printInfo.goods[i].goodName += '  ' + end.join(' - ')
            var fieldValues = GetJsonFields(routes, end)
            if(fields.includes("weight")){
              {
                var w = parseInt(fieldValues["weight"])
                if(!isNaN(w))
                  printInfo.goods[i].weight = w
              }
            }
    
            if(fields.includes("size")){
              printInfo.goods[i].size = fieldValues["size"]
            }
          }

          if(isNaN(printInfo.goods[i].weight) || isNaN(printInfo.goods[i].count)){
            delete printInfo.goods[i]
          }
        }
        
        resolve(printInfo)
    })
  })
}

function GetJsonFields(json, path){
  var j = json;
  var length = path.length
  for(var i = 0; i < length; i++){
    var c = path.shift()
    if(!j.hasOwnProperty(c)){
      if(Array.isArray(j)){
        {
          for(var a = 0; a < j.length; a++){
            var o = j[a]
            if(o.hasOwnProperty(c)){
              j = o[c]
              break;
            }
          }
        }
      }
    }
    else{
      j = j[c]
    }
  }

  var result = {}
  if(!Array.isArray(j))
    return result;

  for(var i = 0; i < j.length; i++){
    o = j[i]
    for(var k in o) {
      if(o[k] != null && o[k] != "")
        result[k]=o[k]
    };
  }

  return result;
}


function getRndInteger(min, max) {
  return (Math.floor(Math.random() * (max - min + 1) ) + min).toString();
}

function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

// page break style="page-break-before: always;"

function htmlGenerator(purchaseInfo){
  counter = 0;
  let html = `
  <html lang="fa" dir="rtl">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style type="text/css">
            body { direction: rtl; }
        </style>
    </head>
    <p style="text-align: center;">گزارش سفارشات</p>
</div>
<div>
<p style="text-align: center;">${ToJalali(purchaseInfo.date)}</p>
</div>
<div>
<table style="text-align: right; width: 542px; height: 86px; margin-left: auto; margin-right: auto;" border=".1" cellspacing="3">
<tbody>
  `;

  html += headerGenerator()
  for(i = 0; i <= purchaseInfo.goods.length; i++)
  {
    if(i == purchaseInfo.goods.length){
      // sum record
      html += sumGenerator(purchaseInfo.goods)
    }
    else
    {
    html += itemGenerator(
      {city: purchaseInfo.city,
      user: purchaseInfo.name,
      mobile: purchaseInfo.number,
      good: purchaseInfo.goods[i].goodName,
      size: purchaseInfo.goods[i].count,
      weight: purchaseInfo.goods[i].weight
      });
    }



    if(i % 25 == 0 && i != 0)
    {
      counter++;
      //break
      html += `
      </tbody>
      </table>
      </div>
      <p style="text-align: center;">${counter}</p>
      
    <p style="text-align: center;page-break-before: always;">گزارش سفارشات</p>
    </div>
    <div>
    <p style="text-align: center;">${ToJalali(purchaseInfo.date)}</p>
    </div>
    <div>
<table style="text-align: right; width: 542px; height: 86px; margin-left: auto; margin-right: auto;" border=".1" cellspacing="3">
<tbody>
      `;
      html+= headerGenerator()
    }

    if(i > purchaseInfo.goods.length)
    {
      counter++;
      //break
      html += `
      </tbody>
      </table>
      </div>
      <p style="text-align: center;">${counter}</p>
      `
    }
  }

  html += `
  </tbody>
  </table>
  </div>
  </html>
  `;

  return html;
}

function headerGenerator(){
  return `
  <tr>
<td style="text-align: center; width: 67px;">
<h3><strong>شهرستان</strong></h3>
</td>
<td style="text-align: center; width: 83px;">
<h3><strong>نام مشتری</strong></h3>
</td>
<td style="text-align: center; width: 97px;">
<h3><strong>شماره تماس</strong></h3>
</td>
<td style="text-align: center; width: 66px;">
<h3><strong>کالا</strong></h3>
</td>
<td style="text-align: center; width: 76px;">
<h3><strong>تعداد</strong></h3>
</td>
<td style="text-align: center; width: 54px;">
<h3><strong>وزن میانگین گرم</strong></h3>
</td>
</tr>
  `
}

function itemGenerator(recordData){
return `
  <tr>
  <td style="width: 67px; text-align: center;">${recordData.city}</td>
  <td style="width: 83px; text-align: center;">${recordData.user}</td>
  <td style="width: 97px; text-align: center;">${recordData.mobile}</td>
  <td style="width: 66px; text-align: center;">${recordData.good}</td>
  <td style="width: 76px; text-align: center;">${TranslateToPersianNumbers(recordData.size.toString())}</td>
  <td style="width: 54px; text-align: center;">${TranslateToPersianNumbers(recordData.weight.toString())}</td>
  </tr>
`
}

function sumGenerator(goods){
  var totalWeight = 0
  for(var i = 0; i < goods.length; i++){
    totalWeight += (goods[i].weight * goods[i].count)
  }

  return `
    <tr>
    <td style="width: 67px; text-align: center;"></td>
    <td style="width: 83px; text-align: center;"></td>
    <td style="width: 97px; text-align: center;"></td>
    <td style="width: 66px; text-align: center;">جمع کل</td>
    <td style="width: 76px; text-align: center;"></td>
    <td style="width: 54px; text-align: center;">${TranslateToPersianNumbers(totalWeight.toString())}</td>
    </tr>
  `
  }

export const createPDF = async (purchase) => {

    requestWritePermission().then(async () => {
      var printInfo = await purchaseToPrint(purchase)

      let options = {
        html: htmlGenerator(printInfo),
        padding: 0,
        height: 842,
        width: 595,
        fileName: 'test',
        directory: 'Documents',
      };

      let file = await RNHTMLtoPDF.convert(options)
   
      android.actionViewIntent(file.filePath, "application/pdf")
    })

};
