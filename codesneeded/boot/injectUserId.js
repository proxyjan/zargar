module.exports = (server) => {

    // Before each create request, assign the userId property to the userId of the accessToken
  
    const addUser = async (ctx) => {
      ctx.req.body.owner = ctx.req.accessToken.userId;
    };
  
    server.models.Good.beforeRemote('create', addUser);
  }