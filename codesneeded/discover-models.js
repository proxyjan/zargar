'use strict';

const loopback = require('loopback');
const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const readFile = promisify(fs.readFile);
const mkdirp = promisify(require('mkdirp'));

const DATASOURCE_NAME = 'db';
const dataSourceConfig = require('./server/datasources.json');
const db = new loopback.DataSource(dataSourceConfig[DATASOURCE_NAME]);

discover().then(
    success => process.exit(),
    error => { console.error('UNHANDLED ERROR:\n', error); process.exit(1); },
  );

  async function discover() {
    const options = {relations: true};

    const categorySchemas = await db.discoverSchemas('category', options);
    const commentSchemas = await db.discoverSchemas('comment', options);
    const favoriteSchemas = await db.discoverSchemas('favorite', options);
    const goodSchemas = await db.discoverSchemas('good', options);
    const hasCategorySchemas = await db.discoverSchemas('hascategory', options);
    const hasVariantSchemas = await db.discoverSchemas('hasvariant', options);
    const likeSchemas = await db.discoverSchemas('likedislike', options);
    const profileSchemas = await db.discoverSchemas('profile', options);
    const purchaseSchemas = await db.discoverSchemas('purchase', options);
    const purchaseGoodsSchemas = await db.discoverSchemas('purchasegoods', options);
    const variantSchemas = await db.discoverSchemas('variant', options);
    
    await mkdirp('common/models');

    await writeFile(
      'common/models/category.json',
      JSON.stringify(categorySchemas['zargar.category'], null, 2)
    );

    await writeFile(
      'common/models/comment.json',
      JSON.stringify(commentSchemas['zargar.comment'], null, 2)
    );

    await writeFile(
      'common/models/favorite.json',
      JSON.stringify(favoriteSchemas['zargar.favorite'], null, 2)
    );

    await writeFile(
      'common/models/good.json',
      JSON.stringify(goodSchemas['zargar.good'], null, 2)
    );

    await writeFile(
      'common/models/hascategory.json',
      JSON.stringify(hasCategorySchemas['zargar.hascategory'], null, 2)
    );

    await writeFile(
      'common/models/hasvariant.json',
      JSON.stringify(hasVariantSchemas['zargar.hasvariant'], null, 2)
    );

    await writeFile(
      'common/models/likedislike.json',
      JSON.stringify(likeSchemas['zargar.likedislike'], null, 2)
    );

    await writeFile(
      'common/models/profile.json',
      JSON.stringify(profileSchemas['zargar.profile'], null, 2)
    );

    await writeFile(
      'common/models/purchase.json',
      JSON.stringify(purchaseSchemas['zargar.purchase'], null, 2)
    );

    await writeFile(
      'common/models/purchasegoods.json',
      JSON.stringify(purchaseGoodsSchemas['zargar.purchasegoods'], null, 2)
    );

    await writeFile(
      'common/models/variant.json',
      JSON.stringify(variantSchemas['zargar.variant'], null, 2)
    );


    // Expose models via REST API
    const configJson = await readFile('server/model-config.json', 'utf-8');
    console.log('MODEL CONFIG', configJson);
    const config = JSON.parse(configJson);

    config.Category = {dataSource: DATASOURCE_NAME, public: true};
    config.Comment = {dataSource: DATASOURCE_NAME, public: true};
    config.Favorite = {dataSource: DATASOURCE_NAME, public: true};
    config.Good = {dataSource: DATASOURCE_NAME, public: true};
    config.Hascategory = {dataSource: DATASOURCE_NAME, public: true};
    config.Hasvariant = {dataSource: DATASOURCE_NAME, public: true};
    config.Likedislike = {dataSource: DATASOURCE_NAME, public: true};
    config.Profile = {dataSource: DATASOURCE_NAME, public: true};
    config.Purchase = {dataSource: DATASOURCE_NAME, public: true};
    config.Purchasegoods = {dataSource: DATASOURCE_NAME, public: true};
    config.Variant = {dataSource: DATASOURCE_NAME, public: true};
 
    await writeFile(
      'server/model-config.json',
      JSON.stringify(config, null, 2)
    );
  }