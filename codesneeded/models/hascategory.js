module.exports = function(Hascategory) {
    

    Hascategory.destory = function(name, goodid, options) {
        const Good = this.app.models.Good;
        return Good.findById(goodid, null, options)
        .then((instance) => {
          const token = options && options.accessToken;
          const userId = token && token.userId;
          if(instance.owner == userId)
          {
              // DELETION
              Hascategory.destroyAll({goodid: goodid, categorynameref: name}, function (err, info) {console.log(err)})

          }
          else
          {
            console.log('didnt matched')
          }
        });
    };
  };