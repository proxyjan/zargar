module.exports = function(Hasvariant) {
    

    Hasvariant.destory = function(goodid, variantid, options) {
        const Good = this.app.models.Good;
        const Variant = this.app.models.Variant;
        return Good.findById(goodid, null, options)
        .then((instance) => {
          const token = options && options.accessToken;
          const userId = token && token.userId;
          if(instance.owner == userId)
          {
              // DELETION
              Hasvariant.destroyAll({goodid: goodid, variantid: variantid}, function (err, info) {console.log(err)})
              Variant.destroyById(variantid , function (err, info) {console.log(err)})

          }
          else
          {
            console.log('didnt matched')
          }
        });
    };
  };