var server = require('./server');
    var ds = server.dataSources.db;
    var tables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role'];

    //var tablesToDiscover = ['something']
    // var updateTables = ['whatever']
    // ds.automigrate(tables, function(er) {
    //     if (er) throw er;
    //     ds.disconnect();
    // });

    ds.autoupdate(tables, function(er) {
        if (er) throw er;
        ds.disconnect();
    });

    // ds.discoverAndBuildModels(
    //     'something',
    //     {relations: true},
    //     function(err, models) {
    //       if (err) return callback(err);
    
    //       // Step 2: expose all new models via REST API
    //       for (const modelName in models) {
    //         app.model(models[modelName], {dataSource: dataSource});
    //       }
    
    //       callback();
    //     });