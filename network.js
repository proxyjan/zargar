import React, { Component, } from 'react';
import {View, Image, Platform, NetInfo, Text, StatusBar} from 'react-native';
import {Colors} from './mainStyles';

var RNFetchBlob = require('rn-fetch-blob').default;

const dirs = RNFetchBlob.fs.dirs;

var startedUrls = []

export class NetworkStats extends React.Component
{
    constructor(props) {
        super(props);

        this.state= {
            type: 'none',
            color: Colors.Back
        }

        this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
    }

    componentDidMount(){

        NetInfo.getConnectionInfo().then((connectionInfo) => {
        });
    
        NetInfo.addEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );
    }

    handleFirstConnectivityChange(connectionInfo) {
        var currentColor = Colors.Back

        if(connectionInfo.type === 'none')
        {
            currentColor = Colors.WeakAlarm;
        }
        else if(connectionInfo.type === 'cellular')
        {
            currentColor = Colors.WeakGold;
        }

        this.setState({type: connectionInfo.type.toString, color: currentColor})
    }

    render()
    {
        return <StatusBar
        translucent ={false}
          backgroundColor = {this.state.color}
          barStyle="dark-content">
          </StatusBar>
    }
}


export class CachedImage extends React.Component
{
    constructor(props) {
        super(props);
    
        this.state = {
            url: this.props.thumbnail,
            progress: 0,
            blur : 1,
            loading: true,
            imagePathTemp: this.props.url ? dirs.CacheDir + '/temp/' + this.getName(this.props.url) : null,
            imagePath: this.props.url?  dirs.CacheDir +  '/' + this.getName(this.props.url) : null,
            thumbnailPathTemp: this.props.thumbnail? dirs.CacheDir + '/temp/' + this.getName(this.props.thumbnail): null,
            thumbnailPath: this.props.thumbnail? dirs.CacheDir +  '/' + this.getName(this.props.thumbnail) : null
        };

        this.getName = this.getName.bind(this)
    }

    getName = (path) => {
        return path.substring(path.lastIndexOf('/')+1);
    }

    
    componentWillReceiveProps(newProps)
    {
        stateModel = {}
        if(newProps.url) {
            stateModel["imagePathTemp"] = dirs.CacheDir + '/temp/' + this.getName(newProps.url)
            stateModel["imagePath"] = dirs.CacheDir +  '/' + this.getName(newProps.url)
        }

        if(newProps.thumbnail){
            stateModel["thumbnailPathTemp"] = dirs.CacheDir + '/temp/' + this.getName(newProps.thumbnail)
            stateModel["thumbnailPath"] = dirs.CacheDir +  '/' + this.getName(newProps.thumbnail)
        }

        this.setState(stateModel, x => this.checkCashe())
    }

    componentDidMount()
    {
        stateModel = {}
        if(this.props.url) {
            stateModel["imagePathTemp"] = dirs.CacheDir + '/temp/' + this.getName(this.props.url)
            stateModel["imagePath"] = dirs.CacheDir +  '/' + this.getName(this.props.url)
        }

        if(this.props.thumbnail){
            stateModel["thumbnailPathTemp"] = dirs.CacheDir + '/temp/' + this.getName(this.props.thumbnail)
            stateModel["thumbnailPath"] = dirs.CacheDir +  '/' + this.getName(this.props.thumbnail)
        }

        this.setState(stateModel, x => this.checkCashe())
    }

    hanldeLoaded() {
        this.setImage()
    }
    
    setImage = () =>{
        this.setState({
            url: (Platform.OS === 'android') ? ('file://' + this.state.imagePath ) : ('' + this.state.imagePath), 
            progress: 1, 
            blur: 0, 
            loading: false});
    }

    setThumbnail = () => {
        if(this.state.loading && this.state.progress < 90)
        {
            this.setState({
                url: (Platform.OS === 'android') ? ('file://' + this.state.thumbnailPath) : ('' + this.state.thumbnailPath)});
        }
    }

    checkCashe =() =>
    {
        if(this.state.imagePath === undefined || this.state.imagePath == null)
            return

        RNFetchBlob.fs.exists(this.state.imagePath)
          .then((exist) => {
              if(exist) {
                this.setImage()
              }
              else {
                this.getImage()
              }
          })
          .catch((error) => { console.log('Checking cache came to this error: \n' + error)
        })
    }

    getImage = () =>
    {
        this.getThumbnail()

        if(this.state.imagePath in startedUrls)
        {
            startedUrls[this.state.imagePath].push(this);
            return;
        }

        startedUrls[this.state.imagePath] =  [this,]

        RNFetchBlob
            .config({
                fileCache : true,
                path : this.state.imagePathTemp
            })
            .fetch('GET', this.props.url)
            .progress((received, total) => {
                this.setState({progress: received / total})
            })
            .then((e) => {
                this.moveToCacheImage()
            })
            .catch((error) => {
                console.log('Error while downloading the image: \n' + error)
            })
    }

    moveToCacheImage = () => {

        RNFetchBlob.fs.mv(this.state.imagePathTemp, this.state.imagePath)
            .then(() => {
                startedUrls[this.state.imagePath].forEach(function(element) {
                    element.hanldeLoaded()
                });
                this.setImage()})
    }

    moveToCacheThumbnail = () => {
        RNFetchBlob.fs.exists(this.state.thumbnailPathTemp)
        .then((exist) => {
            if(exist)
            {
                RNFetchBlob.fs.mv(this.state.thumbnailPathTemp, this.state.thumbnailPath)
                    .then(() => this.setThumbnail())
            }
        })
    }

    getThumbnail = () =>
    {
        RNFetchBlob.fs.exists(this.state.thumbnailPath)
        .then((exist) => {
            if(exist) {
                this.setThumbnail()
            }
            else {
                RNFetchBlob
                .config({
                    fileCache : true,
                    path : this.state.thumbnailPathTemp
                })
                .fetch('GET', this.props.thumbnail)
                .then(() => {
                    this.moveToCacheThumbnail()
                })
            }
        })
    }

    render ()
    {
        return (
            <View style={[{flex: 1 ,justifyContent: 'center', alignContent: 'center', overflow: 'hidden'},this.props.viewStyle]}>
                <Image style ={this.props.style}
                    blurRadius = {this.state.blur} 
                    source={{uri: this.state.url}} />
                {this.state.loading
                ? (<View style ={{width: (this.state.progress * 100).toString() + "%", height: 5, backgroundColor: Colors.WeakGold, alignSelf: 'center'}}/>)
                : (null)}
            </View>
        )
    }
}