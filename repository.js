import {AsyncStorage} from 'react-native';

let  goods = [];
let  goodApiLink = 'http://198.143.181.218:3000/api/Goods';

let profiles = [];
let profileApiLink = 'http://198.143.181.218:3000/api/Profiles';

let categories = [];
let categoryApiLink = 'http://198.143.181.218:3000/api/Categories';

let variantApiLink = 'http://198.143.181.218:3000/api/Variants';
let hasVariantApiLink = 'http://198.143.181.218:3000/api/Hasvariants';
let commentApiLink = 'http://198.143.181.218:3000/api/Comments';
let hasCategoryApiLink = 'http://198.143.181.218:3000/api/Hascategories';

let purchases = [];
let purchaseApiLink = 'http://198.143.181.218:3000/api/Purchases';
let purchaseGoodApiLink = 'http://198.143.181.218:3000/api/Purchasegoods';

let likeDislikeApiLink = 'http://198.143.181.218:3000/api/Likedislikes';

export class GoodsRepository
{
    static PostGood = async (title) =>
    {
        return new Promise(async (resolve, reject) => {
            fetch(goodApiLink + "/" + "?access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    title: title, 
                    owner: 0,
                    createddate: new Date(),
                    modifieddate: new Date()})
            })
            .then((responseJson) => {
                responseJson.json().then((body) => {
                    if(body["error"] != null)
                    {
                        reject(body["error"]["message"])
                    }
                    goods.push(body)
                    resolve(body)
                });
            })
            .catch((error) => {
                console.error(error);
            });
        })
    }

    static getGoods = (page, amount) =>
    {
        return new Promise((resolve, reject) => {
            GoodsRepository.fetchAllGoods().then(()=> {
                resolve(goods)})
                .catch((err) => {

                })
        })
    }

    static goodExist = async (id) =>
    {
        return new Promise((resolve, reject) => {
            fetch(goodApiLink + "/" + id + "/exists" + "?access_token=" + token, {
                method: 'get'
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    static getGood = (id) =>{
        return new Promise((resolve, reject) => {

            var filteredGoods = goods.filter(function(item) {
                return item.id == id
            });

            if(filteredGoods.length <= 0)
            {
                GoodsRepository.goodExist(id).then((exists)=>{
                    if(exists)
                    {
                        fetch(goodApiLink + "/" + id + "?access_token=" + token, {
                            method: 'get'
                        })
                        .then((responseJson) => {
                        responseJson.json().then((body) => {
                            resolve(body)
                        });
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                    }
                    else
                    {
                        reject("not existed")
                    }
                })
            }
            else
            {
                resolve(filteredGoods[0])
            }
        })
    }
    
    static deleteGood = async (id) =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return fetch(goodApiLink + "/" + id + "?access_token=" + token, {
                method: 'delete'
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                goods = goods.filter(function(item) {
                    return item.id != id;
                });
            });
            })
            .catch((error) => {
                reject("Error while deleting good with id of " + id + ". Full error: " + error);
            });
        })
    }

    static fetchAllGoods = async () =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return fetch(goodApiLink + '?filter[order]=createddate%20DESC&filter[include]=likedislikes&filter[include]=comments&filter[include]=favorites&filter[include]=hascategories&filter[include]=variants' + "&access_token=" + token, {
                method: 'GET',
                headers: {
                },
            })
            .then((responseJson) => {
                responseJson.json().then((body) => {
                    goods = body
                });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    static updateGood = async (data) =>
    {
        return new Promise((resolve, reject) => {
            fetch(goodApiLink + "?access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body:JSON.stringify (data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }
}

export class CategoryRepository
{
    static DeleteHasCategory = async (name, goodid) => {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => { 
                console.log(name + ' ' + goodid)
                fetch(hasCategoryApiLink + '/destory?name=' + name + '&goodid=' + goodid + "&access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                },
                method: "POST",
                })
                .then(()=>{
                    resolve('deleted')
                })
            })
        })
    }

    static UpdateHasCategory = async (model) => {
        console.log(JSON.stringify(model))
        return new Promise((resolve, reject) => {
            fetch(hasCategoryApiLink + '/replaceOrCreate', {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(model)
                })
                .then((x)=>{
                    resolve(x)
                })
        })
    }


    static getCategories = async (forced) => {
        return new Promise((resolve, reject) => {
            
            if(forced == true)
            {
                fetch(categoryApiLink, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    categories = body
                    resolve(categories)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            }
            else
            {
                if(categories.length > 0)
                    resolve(categories)
                    
                fetch(categoryApiLink, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    categories = body
                    resolve(categories)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            }
        })
    }

    static PostCategory = async (data) => {
        return new Promise((resolve, reject) => {
            fetch(categoryApiLink, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                categories = body
                resolve(categories)
                });
            })
            .catch((error) => {
                reject("Error while fetching data from API: " + error);
            });
        })
    }

    static PostHasCategory = async (data) => {
        
    }

    static DeleteCategory = async (name, route) => {
        return new Promise((resolve, reject) => { 
            fetch(categoryApiLink + '/destroy?name=' + name + '&route=' + route, {
            headers: {
                'Accept': 'application/json',
            },
            method: "POST",
            })
            .then(()=>{
                resolve('deleted')
            })
        })
    }
}

export class ProfilesRepository
{
    static getProfiles = async () =>
    {
        return new Promise((resolve, reject) => {
            fetch(profileApiLink, {
                method: 'GET',
                headers: {
                },
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                profiles = body
                resolve(profiles)
                });
            })
            .catch((erorr) => {
                reject(erorr)
            });
        })
    }

    static getProfile = (id) =>
    {
        return new Promise((resolve, reject) => {

            var currentProfiles = profiles.filter(function(item) {
                return item.owner == id
            });

            if(currentProfiles.length <= 0)
            {
                ProfilesRepository.getProfiles().then((x) => {
                    var j = x.filter(function(item) {
                        return item.owner == id
                    });

                    if(j.length <= 0)
                        reject('not found');
                    else
                        resolve(j[0])
                })
            }
            else
                resolve(currentProfiles[0])
        })
    }

    static deleteProfile = async (id) =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return fetch(profileApiLink + "/" + id + "?access_token=" + token, {
                method: 'delete'
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                goods = goods.filter(function(item) {
                    return item.id != id;
                });
            });
            })
            .catch((error) => {
                reject("Error while deleting good with id of " + id + ". Full error: " + error);
            });
        })
    }

    static updateProfile = async (data) =>
    {
        return new Promise((resolve, reject) => {
            fetch(profileApiLink + "?access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body:JSON.stringify (data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    static updateProfileUser = async (userId, data) =>
    {
        return new Promise((resolve, reject) => {
            fetch(profileApiLink + "userUpdate?ownerId="+ userId.toString() +"&access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body:JSON.stringify (data)
            })
            .then((responseJson) => {
            responseJson.json().then((text) => {
                resolve(text)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    static create = async (userid) =>
    {
        return new Promise(async (resolve, reject) => {
            fetch(profileApiLink, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify({
                    owner: userid,
                    weight: 0
                })
            })
            .then((responseJson) => {
                responseJson.json().then((body) => {
                    if(body["error"] != null)
                    {
                        reject(body["error"]["message"])
                    }
                    profiles.push(body)
                    resolve(body)
                });
            })
            .catch((error) => {
                console.error(error);
            });
        })
    }   
}

export class VariantRepository
{
    static DeleteVariant = async (id) => {
        return new Promise((resolve, reject) => { 
            fetch(variantApiLink + "/" + id, {
            headers: {
                'Accept': 'application/json',
            },
            method: "DELETE",
            })
            .then((x)=>{
                console.log(x)
                resolve('deleted')
            })
        })
    }


    
    static PostHasVariant = async (data) => {
        return new Promise((resolve, reject) => {
            fetch(hasVariantApiLink, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body)
                });
            })
            .catch((error) => {
                reject("Error while fetching data from API: " + error);
            });
        })
    }

    static PostVariant = async (data) => {
        return new Promise((resolve, reject) => {
            fetch(variantApiLink, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "POST",
                body: JSON.stringify(data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body)
                });
            })
            .catch((error) => {
                reject("Error while fetching data from API: " + error);
            });
        })
    }

    static updateVariant = async (data) =>
    {
        return new Promise((resolve, reject) => {
            fetch(variantApiLink + "?access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body:JSON.stringify (data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }
    
    static updateVariants = async (data) =>
    {
        return new Promise((resolve, reject) => {
            var progress = 0
            data.forEach((model) => {
                VariantRepository.updateVariant(model).then(r=>{
                    progress+= 1

                    console.log(progress)
                })
            })
        })
    }
}

export class CommentRepository
{
    static putComment = async (data) =>
    {
        if(data.message == '')
            data.message = '-'
        return new Promise((resolve, reject) => {
            fetch(commentApiLink + "?access_token=" + token, {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                method: "PUT",
                body:JSON.stringify (data)
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }
}

export class LikeRepository
{
    static PutLikeDislike = async (goodid, owner, isLike) => {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(likeDislikeApiLink + "?access_token=" + token, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "PUT",
                    body:JSON.stringify ({goodid: goodid, islike: isLike, owner: owner})
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                });
                })
                .catch((error) => {
                    reject(error);
                });
            })

        })
    }

    static GetAllLikes = async () =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(likeDislikeApiLink + '?filter[where][islike]=1&access_token=' + token, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            })
        })
    }

    static GetAllDisLikes = async () =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(likeDislikeApiLink + '?filter[where][islike]=0&access_token=' + token, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            })
        })
    }

}

export class PurchaseRepository
{
    static postGoods = async (goods) => {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(purchaseGoodApiLink + "?access_token=" + token, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body:JSON.stringify (goods)
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                });
                })
                .catch((error) => {
                    reject(error);
                });
            })
        })
    }

    static updatePurchase = async (model) =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(purchaseApiLink + "?access_token=" + token, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "PUT",
                    body:JSON.stringify (model)
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                });
                })
                .catch((error) => {
                    reject(error);
                });
            })
        })
    }

    static getPurchases = async () =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(purchaseApiLink + '?filter[order]=resolveddate%20DESC&filter[include][purchasegoods]=goodpurchaseFkrel&filter[include][purchasegoods]=variant&access_token=' + token, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    purchases = body
                    resolve(purchases)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            })
        })
    }

    static getPurchaseGoods = async () =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(purchaseGoodApiLink + '?access_token=' + token, {
                    method: 'GET',
                    headers: {
                    },
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                    });
                })
                .catch((error) => {
                    reject("Error while fetching data from API: " + error);
                });
            })
        })
    }

    static getPurchase = (id) => {
        return new Promise((resolve, reject) => {

            var filteredPurchases = purchases.filter(function(item) {
                return item.id == id
            });

            if(filteredPurchases.length <= 0)
            {
                PurchaseRepository.purchaseExist(id).then((exists)=>{
                    if(exists)
                    {
                        fetch(purchaseApiLink + "/" + id + "?filter[include][purchasegoods]=goodpurchaseFkrel&filter[include][purchasegoods]=variant&access_token=" + token, {
                            method: 'get'
                        })
                        .then((responseJson) => {
                        responseJson.json().then((body) => {
                            purchases.add(body)
                            resolve(body)
                        });
                        })
                        .catch((error) => {
                            console.error(error);
                        });
                    }
                    else
                    {
                        reject('Data doesnt exist in database!')
                    }
                })
            }
            else
            {
                resolve(filteredPurchases[0])
            }
        })
    }

    static purchaseExist = async (id) =>
    {
        return new Promise((resolve, reject) => {
            fetch(purchaseApiLink + "/" + id + "/exists" + "?access_token=" + token, {
                method: 'get'
            })
            .then((responseJson) => {
            responseJson.json().then((body) => {
                resolve(body.exists)
            });
            })
            .catch((error) => {
                reject(error);
            });
        })
    }

    static CreatePurchase = async (data) =>
    {
        return AsyncStorage.getItem("token").then(async (token) => {
            return new Promise((resolve, reject) => {
                fetch(purchaseApiLink + "?access_token=" + token, {
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    method: "POST",
                    body:JSON.stringify (data)
                })
                .then((responseJson) => {
                responseJson.json().then((body) => {
                    resolve(body)
                });
                })
                .catch((error) => {
                    reject(error);
                });
            })
        })
    }
}